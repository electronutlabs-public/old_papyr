Old Papyr
=========

![](papyr_v0.6_actual.jpg)

This repository contains the last working code and Kicad design files for
our older papyr. All the files are for the last iteration of the board, and
there were a few of them. The specifications of this board are:

* STM32F407 Cortex-M4, 168 MHz
* IS62WV51216 SRAM, 2 MB
* TPS65185 power supply for e-paper
* W25Q64FVSSIG SPI flash
* AMW007 wifi module
* MMA7660 accelerometer
* GDE060BA 6" e-paper screen

PCB
===

The PCB design is done in Kicad, and the schematic PDF file along with the
whole of the kicad project is inside the pcb directory.

![](papyr_v0.6_pcb1.png)

Firmware
========

Firmware is present inside [papyr0.6_stm32cubemx](./papyr0.6_stm32cubemx) directory. The planned functionality
is not finished, but it is in a usable state. It is configured and
build using STM32CubeMX + System workbench IDE.
[STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) should be able to import and build it also.

```
papyr0.6_stm32cubemx/
├── Drivers
│   ├── CMSIS/*
│   └── STM32F4xx_HAL_Driver/*
├── Inc
│   ├── array60.h
│   ├── basic_graphics.h
│   ├── BlockDevice.h
│   ├── board.h
│   ├── EPaper.h
│   ├── fatfs.h
│   ├── ffconf.h
│   ├── flash.h
│   ├── FrameBuffer.h
│   ├── FreeRTOSConfig.h
│   ├── jconfig.h
│   ├── jdata_conf.h
│   ├── jmorecfg.h
│   ├── libjpeg.h
│   ├── logger_config.h
│   ├── logger.h
│   ├── logger_internal.h
│   ├── main.h
│   ├── RamFrameBuffer.h
│   ├── SPIFBlockDevice.h
│   ├── stm32f4xx_hal_conf.h
│   ├── stm32f4xx_it.h
│   ├── TPS65185.h
│   ├── ugui_config.h
│   ├── ugui.h
│   ├── usbd_cdc_if.h
│   ├── usbd_conf.h
│   ├── usbd_desc.h
│   ├── usb_device.h
│   └── user_diskio.h
├── Middlewares
│   ├── ST
│   │   └── STM32_USB_Device_Library/*
│   └── Third_Party
│       ├── FatFs/*
│       ├── FreeRTOS/*
│       └── LibJPEG/*
├── openocd.cfg
├── papyr0.6_stm32cubemx.ioc
├── papyr0.6_stm32cubemx.pdf
├── papyr0.6_stm32cubemx.txt
├── papyr0.6_stm32cubemx.xml
├── Src
│   ├── array.c
│   ├── basic_graphics.cpp
│   ├── debug_serial.c
│   ├── EPaper.cpp
│   ├── fatfs.c
│   ├── flash.c
│   ├── freertos.c
│   ├── libjpeg.c
│   ├── logger_internal.c
│   ├── main.cpp
│   ├── RamFrameBuffer.cpp
│   ├── SPIFBlockDevice.cpp
│   ├── stm32f4xx_hal_msp.c
│   ├── stm32f4xx_hal_timebase_TIM.c
│   ├── stm32f4xx_it.c
│   ├── system_stm32f4xx.c
│   ├── test_image.c
│   ├── TPS65185.cpp
│   ├── ugui.c
│   ├── usbd_cdc_if.c
│   ├── usbd_conf.c
│   ├── usbd_desc.c
│   ├── usb_device.c
│   └── user_diskio.c
├── startup
│   └── startup_stm32f407xx.s
└── STM32F407ZETx_FLASH.ld
```
