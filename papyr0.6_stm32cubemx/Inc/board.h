#ifndef __PAPYR_BOARD_H__
#define __PAPYR_BOARD_H__

#include "stm32f4xx_hal.h"
#include "main.h"

/* choose one */
#define BOARD_PAPYR_V0_6
//#define BOARD_DESTM32_L

#if !defined(BOARD_PAPYR_V0_6) && !defined(BOARD_DESTM32_L)
#error "please define a board, see board.h"
#endif

#if defined(BOARD_DESTM32_L)
#define LED_PORT            GPIOB
#define LED_PIN             GPIO_PIN_12
#define HAS_SRAM

#define VNEGGVEE_CTR_GPIO_Port  GPIOB
#define VNEGGVEE_CTR_Pin        GPIO_PIN_9       //PB9           AME5142
#define VPOS15_CTR_GPIO_Port    GPIOG
#define VPOS15_CTR_Pin          GPIO_PIN_14      //PG14          +15V
#define GVDD22_CTR_GPIO_Port    GPIOB
#define GVDD22_CTR_Pin          GPIO_PIN_8       //PB8           +22V

#define E_DB_ODR            GPIOC->ODR
#define E_DB_ODR_MASK       0xff00


#elif defined(BOARD_PAPYR_V0_6)
#define LED_PORT            LED_GREEN_GPIO_Port
#define LED_PIN             LED_GREEN_Pin
#define HAS_SRAM
#define HAS_TPS65185

#define E_DB_ODR            GPIOC->ODR
#define E_DB_ODR_MASK       0xff00

// VCOM_CTR pins already defined, WAKEUP, and PWRUP are needed
#define WAKEUP_GPIO_Port	GPIOG
#define WAKEUP_Pin			GPIO_PIN_11

#define PWRUP_GPIO_Port		GPIOG
#define PWRUP_Pin			GPIO_PIN_13

#endif

#endif
