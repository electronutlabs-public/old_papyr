#ifndef __PAPYR_BLOCK_DEVICE__
#define __PAPYR_BLOCK_DEVICE__

#include <stdint.h>

class BlockDevice
{
public:
    // virtual ~BlockDevice();

    /* methods to get block size in bytes*/
    virtual uint64_t get_read_blocksize() = 0;
    virtual uint64_t get_program_blocksize() = 0;
    virtual uint64_t get_erase_blocksize() = 0;

    /* total size of block device in bytes */
    virtual uint64_t size() = 0;

    virtual int init() = 0;
    virtual int deinit() = 0;

    /* addr is address of block(not block number), and size
       must be a multiple of corresponding block size */
    virtual int read(const void *buf, uint64_t addr, uint64_t size) = 0;
    virtual int program(const void *buf, uint64_t addr, uint64_t size) = 0;
    virtual int erase(uint64_t addr, uint64_t size) = 0;
};

#endif
