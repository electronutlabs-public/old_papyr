/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#include "board.h"
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define E_SPH_Pin GPIO_PIN_3
#define E_SPH_GPIO_Port GPIOE
#define E_OE_Pin GPIO_PIN_4
#define E_OE_GPIO_Port GPIOE
#define E_CLK_Pin GPIO_PIN_5
#define E_CLK_GPIO_Port GPIOE
#define E_LE_Pin GPIO_PIN_6
#define E_LE_GPIO_Port GPIOE
#define S_A0_Pin GPIO_PIN_0
#define S_A0_GPIO_Port GPIOF
#define S_A1_Pin GPIO_PIN_1
#define S_A1_GPIO_Port GPIOF
#define S_A2_Pin GPIO_PIN_2
#define S_A2_GPIO_Port GPIOF
#define S_A3_Pin GPIO_PIN_3
#define S_A3_GPIO_Port GPIOF
#define S_A4_Pin GPIO_PIN_4
#define S_A4_GPIO_Port GPIOF
#define S_A5_Pin GPIO_PIN_5
#define S_A5_GPIO_Port GPIOF
#define E_STV_Pin GPIO_PIN_6
#define E_STV_GPIO_Port GPIOF
#define E_GMODE1_Pin GPIO_PIN_7
#define E_GMODE1_GPIO_Port GPIOF
#define E_CPV_Pin GPIO_PIN_8
#define E_CPV_GPIO_Port GPIOF
#define AMW_GPIO0_Pin GPIO_PIN_9
#define AMW_GPIO0_GPIO_Port GPIOF
#define E_GMODE2_Pin GPIO_PIN_10
#define E_GMODE2_GPIO_Port GPIOF
#define E_D0_Pin GPIO_PIN_0
#define E_D0_GPIO_Port GPIOC
#define E_D1_Pin GPIO_PIN_1
#define E_D1_GPIO_Port GPIOC
#define E_D2_Pin GPIO_PIN_2
#define E_D2_GPIO_Port GPIOC
#define E_D3_Pin GPIO_PIN_3
#define E_D3_GPIO_Port GPIOC
#define SPIF_NSS_Pin GPIO_PIN_4
#define SPIF_NSS_GPIO_Port GPIOA
#define SPIF_SCK_Pin GPIO_PIN_5
#define SPIF_SCK_GPIO_Port GPIOA
#define SPIF_MISO_Pin GPIO_PIN_6
#define SPIF_MISO_GPIO_Port GPIOA
#define SPIF_MOSI_Pin GPIO_PIN_7
#define SPIF_MOSI_GPIO_Port GPIOA
#define E_D4_Pin GPIO_PIN_4
#define E_D4_GPIO_Port GPIOC
#define E_D5_Pin GPIO_PIN_5
#define E_D5_GPIO_Port GPIOC
#define UART2_RESET_Pin GPIO_PIN_0
#define UART2_RESET_GPIO_Port GPIOB
#define AMW_GPIO2_Pin GPIO_PIN_11
#define AMW_GPIO2_GPIO_Port GPIOF
#define S_A6_Pin GPIO_PIN_12
#define S_A6_GPIO_Port GPIOF
#define S_A7_Pin GPIO_PIN_13
#define S_A7_GPIO_Port GPIOF
#define S_A8_Pin GPIO_PIN_14
#define S_A8_GPIO_Port GPIOF
#define S_A9_Pin GPIO_PIN_15
#define S_A9_GPIO_Port GPIOF
#define S_A10_Pin GPIO_PIN_0
#define S_A10_GPIO_Port GPIOG
#define S_A11_Pin GPIO_PIN_1
#define S_A11_GPIO_Port GPIOG
#define S_D4_Pin GPIO_PIN_7
#define S_D4_GPIO_Port GPIOE
#define S_D5_Pin GPIO_PIN_8
#define S_D5_GPIO_Port GPIOE
#define S_D6_Pin GPIO_PIN_9
#define S_D6_GPIO_Port GPIOE
#define S_D7_Pin GPIO_PIN_10
#define S_D7_GPIO_Port GPIOE
#define S_D8_Pin GPIO_PIN_11
#define S_D8_GPIO_Port GPIOE
#define S_D9_Pin GPIO_PIN_12
#define S_D9_GPIO_Port GPIOE
#define S_D10_Pin GPIO_PIN_13
#define S_D10_GPIO_Port GPIOE
#define S_D11_Pin GPIO_PIN_14
#define S_D11_GPIO_Port GPIOE
#define S_D12_Pin GPIO_PIN_15
#define S_D12_GPIO_Port GPIOE
#define SPI_SLAVE_SCK_Pin GPIO_PIN_10
#define SPI_SLAVE_SCK_GPIO_Port GPIOB
#define LED_RED_Pin GPIO_PIN_11
#define LED_RED_GPIO_Port GPIOB
#define LED_GREEN_Pin GPIO_PIN_12
#define LED_GREEN_GPIO_Port GPIOB
#define LED_BLUE_Pin GPIO_PIN_13
#define LED_BLUE_GPIO_Port GPIOB
#define SPI_SLAVE_MISO_Pin GPIO_PIN_14
#define SPI_SLAVE_MISO_GPIO_Port GPIOB
#define SP_SLAVE_MOSI_Pin GPIO_PIN_15
#define SP_SLAVE_MOSI_GPIO_Port GPIOB
#define S_D13_Pin GPIO_PIN_8
#define S_D13_GPIO_Port GPIOD
#define S_D14_Pin GPIO_PIN_9
#define S_D14_GPIO_Port GPIOD
#define S_D15_Pin GPIO_PIN_10
#define S_D15_GPIO_Port GPIOD
#define S_A16_Pin GPIO_PIN_11
#define S_A16_GPIO_Port GPIOD
#define S_A17_Pin GPIO_PIN_12
#define S_A17_GPIO_Port GPIOD
#define S_A18_Pin GPIO_PIN_13
#define S_A18_GPIO_Port GPIOD
#define S_D0_Pin GPIO_PIN_14
#define S_D0_GPIO_Port GPIOD
#define S_D1_Pin GPIO_PIN_15
#define S_D1_GPIO_Port GPIOD
#define S_A12_Pin GPIO_PIN_2
#define S_A12_GPIO_Port GPIOG
#define S_A13_Pin GPIO_PIN_3
#define S_A13_GPIO_Port GPIOG
#define S_A14_Pin GPIO_PIN_4
#define S_A14_GPIO_Port GPIOG
#define S_A15_Pin GPIO_PIN_5
#define S_A15_GPIO_Port GPIOG
#define E_D6_Pin GPIO_PIN_6
#define E_D6_GPIO_Port GPIOC
#define E_D7_Pin GPIO_PIN_7
#define E_D7_GPIO_Port GPIOC
#define I2C_SDA_Pin GPIO_PIN_9
#define I2C_SDA_GPIO_Port GPIOC
#define I2C_SCL_Pin GPIO_PIN_8
#define I2C_SCL_GPIO_Port GPIOA
#define UART1_TX_Pin GPIO_PIN_9
#define UART1_TX_GPIO_Port GPIOA
#define UART1_RX_Pin GPIO_PIN_10
#define UART1_RX_GPIO_Port GPIOA
#define nINT_Pin GPIO_PIN_11
#define nINT_GPIO_Port GPIOC
#define nINT1_Pin GPIO_PIN_12
#define nINT1_GPIO_Port GPIOC
#define S_D2_Pin GPIO_PIN_0
#define S_D2_GPIO_Port GPIOD
#define S_D3_Pin GPIO_PIN_1
#define S_D3_GPIO_Port GPIOD
#define BUTTON2_Pin GPIO_PIN_2
#define BUTTON2_GPIO_Port GPIOD
#define BUTTON4_Pin GPIO_PIN_3
#define BUTTON4_GPIO_Port GPIOD
#define S_OE_Pin GPIO_PIN_4
#define S_OE_GPIO_Port GPIOD
#define S_WE_Pin GPIO_PIN_5
#define S_WE_GPIO_Port GPIOD
#define BUTTON3_Pin GPIO_PIN_6
#define BUTTON3_GPIO_Port GPIOD
#define BUTTON1_Pin GPIO_PIN_7
#define BUTTON1_GPIO_Port GPIOD
#define S_CE_Pin GPIO_PIN_10
#define S_CE_GPIO_Port GPIOG
#define VCOM_CTRL_Pin GPIO_PIN_12
#define VCOM_CTRL_GPIO_Port GPIOG
#define VGS_Pin GPIO_PIN_15
#define VGS_GPIO_Port GPIOG
#define SPI_SLAVE_CS_Pin GPIO_PIN_9
#define SPI_SLAVE_CS_GPIO_Port GPIOB
#define S_LB_Pin GPIO_PIN_0
#define S_LB_GPIO_Port GPIOE
#define S_UB_Pin GPIO_PIN_1
#define S_UB_GPIO_Port GPIOE

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */
#if defined(__cplusplus)
extern "C"{
#endif
void _Error_Handler(char *, int);
#if defined(__cplusplus)
}
#endif

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
