/** @file TPS65185.h
 *
 * @brief Class for epaper power managemnt chip
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/

#ifndef _TPS65185_H
#define _TPS65185_H

#include <stdint.h>
#include <stm32f4xx_hal.h>

// register defs
#define TPS65185_TMST_VALUE    0x00
#define TPS65185_ENABLE        0x01
#define TPS65185_VADJ          0x02
#define TPS65185_VCOM1         0x03 
#define TPS65185_VCOM2         0x04
#define TPS65185_INT_EN1       0x05
#define TPS65185_INT_EN2       0x06
#define TPS65185_INT1          0x07
#define TPS65185_INT2          0x08
#define TPS65185_UPSEQ0        0x09
#define TPS65185_UPSEQ1        0x0A
#define TPS65185_DWNSEQ0       0x0B
#define TPS65185_DWNSEQ1       0x0C
#define TPS65185_TMST1         0x0D
#define TPS65185_TMST2         0x0E
#define TPS65185_PG            0x0F
#define TPS65185_REVID         0x10

// I2C address
#define TPS65185_I2C_ADDR      0x68<<1

class TPS65185
{

public:

    // constr/destr
    TPS65185(I2C_HandleTypeDef *hi2c);
    virtual ~TPS65185() {}

    // methods

    // initialise
    void init();
    
    // put in ACTIVE mode
    void makeActive();

    // put in STANDBY mode
    void makeStandby();

private:
    I2C_HandleTypeDef *_hi2c;

    // read register
    uint8_t _readRegister(uint8_t reg);

    // write to register
    void _writeRegister(uint8_t reg, uint8_t value);

    // set VCOM voltage (negative, mV) in VCOM1
    void _setVCOMVoltage(int voltageMV);

};

#endif // _TPS65185_H
