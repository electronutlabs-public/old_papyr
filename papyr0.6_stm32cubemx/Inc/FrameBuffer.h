/** @file FrameBuffer.h
 *
 * @brief FrameBuffer class interface
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/ 

#ifndef __PAPYR_FRAMEBUFFER_H
#define __PAPYR_FRAMEBUFFER_H

#include <stdint.h>

// these are framebuffer and image types
enum
{
    IMAGE_TYPE_MONOCHROME,
    IMAGE_TYPE_MONOCHROME_RAW,
    IMAGE_TYPE_GS4,
    IMAGE_TYPE_GS16,
};

// Single channel framebuffer class
class FrameBuffer
{
public:
//	virtual ~FrameBuffer() = 0;
    virtual uint8_t bits_per_pixel() = 0;
    virtual uint16_t width() = 0;
    virtual uint16_t height() = 0;
    // this is present apart from bits_per_pixel because 
    // e.g. IMAGE_TYPE_GS4 and IMAGE_TYPE_MONOCHROME_RAW have
    // same bits_per_pixel() but different representation / depth
    virtual uint8_t type() = 0;

    virtual uint8_t* raw_ptr() = 0;
    virtual uint32_t size() = 0;

    virtual void putpixel(uint16_t x, uint16_t y, uint8_t color) = 0;
    virtual void clear(uint8_t color) = 0;
};

#endif
