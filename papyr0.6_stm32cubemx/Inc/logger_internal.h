#ifndef __LOGGER_INTERNAL_H
#define __LOGGER_INTERNAL_H

#include "logger.h"

#define LOG_LEVEL_DEFAULT 0U
#define LOG_LEVEL_ERROR   1U
#define LOG_LEVEL_WARN    2U
#define LOG_LEVEL_INFO    3U
#define LOG_LEVEL_DEBUG   4U

#define IS_POWER_OF_TWO(A) ( ((A) != 0) && ((((A) - 1) & (A)) == 0) )
#define COMPILE_TIME_ASSERT(expr) {typedef char COMP_TIME_ASSERT[(expr) ? 1 : 0];}

// this header precedes any actual data on the buffer
typedef struct
{
    uint32_t type : 2;
    uint32_t raw : 1;
    uint32_t severity : 3;
    uint32_t length : 26;
#if defined(LOG_ENABLE_TIMESTAMP)
    uint32_t time_ms;
#endif
}log_header_t;

//COMPILE_TIME_ASSERT(IS_POWER_OF_TWO(LOG_BUFSIZE))

// This is the control block for logger internals
typedef struct
{
    uint32_t wr_index;
    uint32_t rd_index; // used for both advancing on overflow, or deqeing
    uint32_t mask;     // size of the buffer
    uint8_t *buffer[LOG_BUFSIZE];
}log_cb_t;

#endif
