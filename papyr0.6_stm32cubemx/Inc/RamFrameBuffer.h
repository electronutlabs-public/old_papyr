/** @file RamFrameBuffer.h
 *
 * @brief RamFrameBuffer class interface
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/ 

#ifndef __RAM_FRAMEBUFFER_H__
#define __RAM_FRAMEBUFFER_H__

#include <FrameBuffer.h>

class RamFrameBuffer : public FrameBuffer
{
public:
    RamFrameBuffer(uint8_t *fb_addr, uint8_t t, uint16_t w, int16_t h);

    uint8_t bits_per_pixel();
    uint16_t width();
    uint16_t height();
    uint8_t type();

    uint8_t* raw_ptr();
    uint32_t size();
    void clear(uint8_t color=0);
    
    inline void putpixel(uint16_t x, uint16_t y, uint8_t color)
    {
        uint8_t bpp = bits_per_pixel();

        if((x>_width) || (y>_height))
        {
            return;
        }

        int byte_offset = y*100*bpp + (x*bpp)/8;
        int bit_offset = (x*bpp)%8;

        uint8_t *pixel = &_addr[byte_offset];
        uint8_t masked_color = (((1<<bpp)-1) & color) << bit_offset;
        uint8_t mask = ((1<<bpp)-1) << bit_offset;

        *pixel = (*pixel&~mask) | masked_color;
    }

public:
    // extra methods
    void set_addr(uint8_t * addr);
    void width(uint16_t w);
    void height(uint16_t h);
    void type(uint8_t t);

private:
    uint8_t *_addr;
    uint8_t _type;
    uint16_t _width;
    uint16_t _height;
};

#endif
