#ifndef __CERIAL_FLASH_H
#define __CERIAL_FLASH_H

/*
Generic winbond like SPI flash driver
based on code from https://github.com/PaulStoffregen/SerialFlash 
which is: Copyright (C) 2015, Paul Stoffregen, paul@pjrc.com
*/
#include <stdint.h>
#include <stddef.h>

#if defined(__cplusplus)
extern "C" {
#endif

#define RET_OK 0
#define RET_ERR -1

typedef struct _spi_interface
{
    //  initialize SPI bus, returns 0 on success
    int (*init)(struct _spi_interface *);
    // releases SPI bus, returns 0 on success
    int (*deinit)(struct _spi_interface *);
    // spi data txfr functions
    void (*begin_tx)(struct _spi_interface *);  // spi should be set to MSBFIRST, SPI_MODE0
    void (*end_tx)(struct _spi_interface *);
    uint8_t (*tx)(struct _spi_interface *, uint8_t data);
    uint16_t (*tx16)(struct _spi_interface *, uint16_t data);
    void (*tx_buf)(struct _spi_interface *, uint8_t *buf, uint8_t *rdbuf, size_t count); // buf or rdbuf can be null
    // chip select functions
    void (*cs_assert)(struct _spi_interface *);
    void (*cs_release)(struct _spi_interface *);
    // delay function
    void (*delay_us)(struct _spi_interface *, uint32_t);
    
    // chip JEDEC ID
    uint8_t id[5];
    uint8_t flags;	// chip features
    uint8_t busy;
    // 1 = suspendable program operation
    // 2 = suspendable erase operation
    // 3 = busy

    // any extra information that may be required by underlying driver, like SS, or SPI driver struct
    void * extra;
}spi_interface;

int sf_begin(spi_interface * spi_if);
int sf_end(spi_interface * spi_if);

uint32_t sf_capacity(spi_interface * spi_if, const uint8_t *id);
uint32_t sf_blockSize(spi_interface * spi_if);
void sf_sleep(spi_interface * spi_if);
void sf_wakeup(spi_interface * spi_if);
void sf_readID(spi_interface * spi_if, uint8_t *buf);
void sf_readSerialNumber(spi_interface * spi_if, uint8_t *buf);
void sf_read(spi_interface * spi_if, uint32_t addr, uint8_t *buf, uint32_t len);
int sf_ready(spi_interface * spi_if);
void sf_wait(spi_interface * spi_if);
void sf_write(spi_interface * spi_if, uint32_t addr, const uint8_t *buf, uint32_t len);
void sf_eraseAll(spi_interface * spi_if);
void sf_eraseBlock(spi_interface * spi_if, uint32_t addr);

#if defined(__cplusplus)
}
#endif

#endif
