#ifndef __LOGGER_COnFIG__H
#define __LOGGER_COnFIG__H

/* Log events >= this level */
#define LOG_LEVEL LOG_LEVEL_INFO
#define LOG_ENABLE_TIMESTAMP 

// #define LOG_BACKEND_USE_STDERR 
// #define LOG_BACKEND_USE_SERIAL_DMA 
#define LOG_BACKEND_USE_RAM_BUFFER  

#define LOG_BUFSIZE                 2048  // must be a power of two, can be smaller for deferred
#define LOG_ENABLE_COLOR

#endif
