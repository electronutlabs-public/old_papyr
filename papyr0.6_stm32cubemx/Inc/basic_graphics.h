/** @file basic_graphics.h
 *
 * @brief Graphics drawing functions to test epaper
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/

#ifndef PAPYR_BASIC_GRAPHICS_H
#define PAPYR_BASIC_GRAPHICS_H

#include <FrameBuffer.h>

void  mandelbrot(FrameBuffer *fb, int iterations=15, uint8_t color=1);

void circle(FrameBuffer *fb, int x0, int y0, int radius, uint8_t col=0);
void line(FrameBuffer *fb, int x0, int y0, int x1, int y1, uint8_t col=0);

#endif
