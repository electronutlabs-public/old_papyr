/** @file SPIFBlockDevice.h
 *
 * @brief SPIFBlockDevice class is block device layer for SPI flash chips
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/ 

#ifndef __PAPYR_SPIFBD_H__
#define __PAPYR_SPIFBD_H__

#include <stm32f4xx_hal.h>
#include "flash.h"
#include "BlockDevice.h"

struct spi_extra
{
    GPIO_TypeDef *ss_Port; 
    uint16_t ss_Pin;
    SPI_HandleTypeDef *hspi;
};

class SPIFBlockDevice : public BlockDevice
{
public:
    SPIFBlockDevice(SPI_HandleTypeDef *hspi, GPIO_TypeDef *ss_port, uint16_t pin);

    int init();
    int deinit();

    uint64_t get_read_blocksize();
    uint64_t get_program_blocksize();
    uint64_t get_erase_blocksize();

    uint64_t size();

    int read(const void *buf, uint64_t addr, uint64_t size);
    int program(const void *buf, uint64_t addr, uint64_t size);
    int erase(uint64_t addr, uint64_t size);

private:
    spi_interface _spi_if;
    struct spi_extra _spi_extra_info;
};

#endif
