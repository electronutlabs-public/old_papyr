#ifndef _EPAPER_H_
#define _EPAPER_H_

#include <FrameBuffer.h>
#include <stdint.h>
#include <stm32f4xx_hal.h>
#include "main.h"
#include "board.h"
#include "TPS65185.h"
#include "BlockDevice.h"
#include "array60.h"


#define WHITE_SEQ	0xaa
#define BLACK_SEQ	0x55

// TPS65185 epaper power chip
#if defined(HAS_TPS65185)
#if defined(BOARD_PAPYR_V0_6)
#define I2C_HANDLE_T hi2c3
#endif
extern I2C_HandleTypeDef I2C_HANDLE_T;
#endif // defined(HAS_TPS65185)

#define DEFAULT_DISP_DELAY  30

class EPaper
{
public:   
    // constr/destr
    EPaper();
    // methods
    
    // initialise display
    void init();
    
    // power on sequence
    void power_on();
    
    // power off sequence
    void power_off();
    
    // display image
    void display_test();
    
    // display an image from pointer (4 level grayscale data)
    void show_image_gs4(const uint8_t *);
    
    void show_image_monochrome(const uint8_t *ptr);
    
    void show_image_monochrome_raw(const uint8_t *ptr);
    
    void show_block_device_image(BlockDevice &bd, uint64_t address, uint8_t type = IMAGE_TYPE_MONOCHROME_RAW);
    
    void clear();
    
    void pin_test();
    
    // epaper size definition
    unsigned int epaper_row_pixels, epaper_rows;
    
    void attach_fb(FrameBuffer *_fb);

    void draw_fb();
    
    FrameBuffer *fb;
    
private: //methods
    
    // create wave table
    void _make_wave_table(void);
    
    void _map_frame_gs4_rowdata(const uint8_t *new_pic, uint8_t frame);
    
    void _map_frame_monochrome_rowdata(const uint8_t *new_pic, uint8_t frame);
    
    // start a scan line
    void _start_scan();
    
    void _send_row_data(const unsigned char *pArray, unsigned int disp_delay=DEFAULT_DISP_DELAY);
    
    void _vclock_quick();
    
private:
    // buffer used for writing rows by some internal functions
    uint8_t g_dest_data[200];
    
    static uint8_t black[200];
	static uint8_t white[200];

    #if defined(HAS_TPS65185)
    TPS65185 _tps65185;
    #endif

};

#endif // _EPAPER_H_
