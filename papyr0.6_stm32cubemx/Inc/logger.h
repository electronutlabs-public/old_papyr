/** @file logger.h
 *
 * @brief logging backend selection
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/ 

#ifndef __PAPYR_LOGGER_H
#define __PAPYR_LOGGER_H

#include <stdint.h>
#include "logger_internal.h"
#include "logger_config.h"

/* 
    Write's to stderr with _write, flash, or to a circular buffer in ram.
    If you are using LOG_BACKEND_USE_RAM_BUFFER, you can deque log entries in your own code
*/
#if !defined(LOG_BACKEND_USE_STDERR) && !defined(LOG_BACKEND_USE_SERIAL_DMA) && !defined(LOG_BACKEND_USE_RAM_BUFFER)
#warning "No logging backend selected"
#endif

#if !defined(LOG_LEVEL)
#define LOG_LEVEL   LOG_LEVEL_INFO
#endif

#if !defined(LOG_BUFSIZE)
#define LOG_BUFSIZE 256
#endif

// define module name before including this file
#ifndef LOG_MODULE_NAME
#define LOG_MODULE_NAME ""
#endif

#endif
