#ifndef __ARRAY60_H
#define __ARRAY60_H	

#if 0
#define FRAME_GS16_LEN_60  22
#define FRAME_GS16_DEPTH   16
static unsigned char wave_gs16_60[FRAME_GS16_DEPTH][FRAME_GS16_LEN_60]=
{
    0,1,1,2,2, 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,0,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,1,1,1, 1,1,1,1, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,1,1,1, 1,1,1,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,1,1,1, 1,1,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,1,1,1, 1,0,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,1,1,1, 0,0,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,1,1,0, 0,0,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,1,0,0, 0,0,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,1,1, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,1,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,1,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,
    0,1,1,2,2, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,
};
#endif

#if 1
#define FRAME_GS4_LEN_60  15
#define FRAME_GS4_DEPTH   4
static unsigned char wave_gs4_60[FRAME_GS4_DEPTH][FRAME_GS4_LEN_60]=
{
    {1,1,1,1,2,2,2,2,1,1,1,1,1,1,1},    //GC3->GC0
    {1,1,1,1,2,2,2,2,1,1,1,1,0,0,0},    //GC3->GC1
    {1,1,1,1,2,2,2,2,1,1,0,0,0,0,0},    //GC3->GC2
    {1,1,1,1,2,2,2,2,2,0,0,0,0,0,0},    //GC3->GC3
};
#else
#define FRAME_GS4_LEN_60  7
#define FRAME_GS4_DEPTH   4
static unsigned char wave_gs4_60[FRAME_GS4_DEPTH][FRAME_GS4_LEN_60]=
{
    {2,2,2,1,1,1,0},    //GC3->GC0
    {2,2,2,1,1,0,0},    //GC3->GC1
    {2,2,2,1,0,0,0},    //GC3->GC2
    {2,2,2,0,0,0,0},    //GC3->GC3
};
#endif

#if 0
#define FRAME_MONOCHROME_LEN_60  12
#define FRAME_MONOCHROME_DEPTH   2
static unsigned char wave_monochrome_60[FRAME_MONOCHROME_DEPTH][FRAME_MONOCHROME_LEN_60]=
{
    {0,1,1,1,1,2,2,2,2,1,1,1},
    {0,2,2,2,1,1,1,1,2,2,2,2,},
};
#else
#define FRAME_MONOCHROME_LEN_60  10
#define FRAME_MONOCHROME_DEPTH   2
static unsigned char wave_monochrome_60[FRAME_MONOCHROME_DEPTH][FRAME_MONOCHROME_LEN_60]=
{
    {2,2,1,1,2,2,1,1,1,1},	//GC1->GC0
    {2,2,1,1,2,2,2,2,0,0},  //GC1->GC1
};
#endif

#define FRAME_MONOCHROME_RAW_REPEAT	6

#endif
