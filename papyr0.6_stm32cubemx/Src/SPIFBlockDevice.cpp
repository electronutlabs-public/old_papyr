/** @file SPIFBlockDevice.cpp
 *
 * @brief SPIFBlockDevice class is block device layer for SPI flash chips
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/ 

#include <stm32f4xx_hal.h>
#include "flash.h"
#include "SPIFBlockDevice.h"

static int spi_init(spi_interface *spi_if);
static int spi_deinit(spi_interface *spi_if);
static void spi_begin_tx(spi_interface *spi_if);
static void spi_end_tx(spi_interface *spi_if);
static uint8_t spi_tx(spi_interface *spi_if, uint8_t data);
static uint16_t spi_tx16(spi_interface *spi_if, uint16_t data);
static void spi_tx_buf(spi_interface *spi_if, uint8_t *buf, uint8_t *rdbuf, size_t count);
static void spi_cs_assert(spi_interface *spi_if);
static void spi_cs_release(spi_interface *spi_if);
static void delay_us(spi_interface *spi_if, uint32_t us);

SPIFBlockDevice::SPIFBlockDevice(SPI_HandleTypeDef *hspi, GPIO_TypeDef *ss_port, uint16_t pin)
{
    _spi_if.init = spi_init;
    _spi_if.deinit = spi_deinit;
    _spi_if.begin_tx = spi_begin_tx;
    _spi_if.end_tx = spi_end_tx;
    _spi_if.tx = spi_tx;
    _spi_if.tx16 = spi_tx16;
    _spi_if.tx_buf = spi_tx_buf;
    _spi_if.cs_assert = spi_cs_assert;
    _spi_if.cs_release = spi_cs_release;
    _spi_if.delay_us = delay_us;

    _spi_extra_info.ss_Port = ss_port;
    _spi_extra_info.ss_Pin = pin;
    _spi_extra_info.hspi = hspi;

    _spi_if.extra = (void *)&_spi_extra_info;
}

int SPIFBlockDevice::init()
{
    if(sf_begin(&_spi_if) != RET_OK)
    {
        return -1;
    }
    
    return 0;
}

int SPIFBlockDevice::deinit()
{
    if(sf_end(&_spi_if) != RET_OK)
    {
        return -1;
    }
    
    return 0;
}

uint64_t SPIFBlockDevice::get_read_blocksize()
{
    return 1;
}

uint64_t SPIFBlockDevice::get_program_blocksize()
{
    return 1;
}

uint64_t SPIFBlockDevice::get_erase_blocksize()
{
    return sf_blockSize(&_spi_if);
}

int SPIFBlockDevice::read(const void *buf, uint64_t addr, uint64_t size)
{
    sf_read(&_spi_if, addr, (uint8_t*)buf, size);
    return 0;
}

int SPIFBlockDevice::program(const void *buf, uint64_t addr, uint64_t size)
{
    sf_write(&_spi_if, addr, (uint8_t*)buf, size);
    return 0;
}

int SPIFBlockDevice::erase(uint64_t addr, uint64_t size)
{
	uint32_t chunk = get_erase_blocksize();
	while (size > 0)
	{
		sf_eraseBlock(&_spi_if, addr);
		addr += chunk;
		if(size > chunk)
		{
			size -= chunk;
		}
		else
		{
			size = 0;
		}
	}

    sf_eraseBlock(&_spi_if, addr);

    return 0;
}

uint64_t SPIFBlockDevice::size()
{
    return sf_capacity(&_spi_if, _spi_if.id);
}

/*--------------------------------------*/
/* C spi flash code interface functions */

/* can't be a class member */
static void delay_us (spi_interface *spi_if, uint32_t us)
{
    for ( us *= 168/6;us>0;us--)
    {
        __asm__("  nop;");
    }
}


static int spi_init(spi_interface *spi_if)
{
    struct spi_extra *spi_info = (struct spi_extra *)spi_if->extra;
    // set SS pin as output
    // should already be set to output, but setting anyway
    GPIO_InitTypeDef g;
    g.Pin = spi_info->ss_Pin;
    g.Mode = GPIO_MODE_OUTPUT_PP;
    g.Pull = GPIO_PULLUP;
    g.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(spi_info->ss_Port, &g);

    return RET_OK;
}


static int spi_deinit(spi_interface *spi_if)
{
    return RET_OK;
}

static void spi_begin_tx(spi_interface *spi_if)
{
    // nothing to do
}

static void spi_end_tx(spi_interface *spi_if)
{
    // nothing to do
}

static uint8_t spi_tx(spi_interface *spi_if, uint8_t data)
{
    struct spi_extra *spi_info = (struct spi_extra *)spi_if->extra;
    uint8_t rdata;
    // NRF_LOG_RAW_INFO("SPI_TX  : %8X", data);
    if(HAL_SPI_TransmitReceive(spi_info->hspi, &data, &rdata, 1, 10000) != HAL_OK)
    {
        // error
    }
    // NRF_LOG_RAW_INFO(" %8X\r\n", rdata);
    return rdata;
}

static uint16_t spi_tx16(spi_interface *spi_if, uint16_t data)
{
    struct spi_extra *spi_info = (struct spi_extra *)spi_if->extra;
    uint16_t rdata;
    // NRF_LOG_RAW_INFO("SPI_TX16: %8X", data);
    // nrf_drv_spi_transfer(&m_spi_master_0, (uint8_t*)&data, 2, (uint8_t*)&rdata, 2);
//    if(HAL_SPI_TransmitReceive(spi_info->hspi, (uint8_t *)&data, (uint8_t *)&rdata, 2, 10000) != HAL_OK)
//    {
//        // error
//    }
     for(int i=1; i>=0; i--)
//    for(int i=0; i<2; i++)
     {
     	if(HAL_SPI_TransmitReceive(spi_info->hspi, ((uint8_t *)&data)+i, ((uint8_t *)&rdata)+i, 1, 10000) != HAL_OK)
         {
             // error
         }
     }
    // NRF_LOG_RAW_INFO(" %8X\r\n", rdata);
    return rdata;
}

static void spi_tx_buf(spi_interface *spi_if, uint8_t *buf, uint8_t *rdbuf, size_t count)
{
    struct spi_extra *spi_info = (struct spi_extra *)spi_if->extra;
    size_t sent=0;
    while(count>0)
    {
        uint16_t blen;
        if(count > 0xffff)
        {
            blen = 0xffff;
        }
        else
        {
            blen = count;
        }

        // NRF_LOG_RAW_INFO("RX: %d\r\n", blen);
       if(buf == NULL)
       {
    	   if(rdbuf == NULL)
    	   {
    		   //invalid input, both buf and rdbuf can't be NULL
    	   }
    	   else
    	   {
			   if(HAL_SPI_Receive(spi_info->hspi, rdbuf+sent, blen, 10000) != HAL_OK)
			   {
				   // error
			   }
    	   }
       }
       else
       {
    	   if(rdbuf == NULL)
		   {
    		   if(HAL_SPI_Transmit(spi_info->hspi, buf+sent, blen, 10000) != HAL_OK)
			   {
				   // error
			   }
		   }
    	   else
    	   {
			   if(HAL_SPI_TransmitReceive(spi_info->hspi, buf+sent, rdbuf+sent, blen, 10000) != HAL_OK)
			   {
				   // error
			   }
    	   }
       }

        count -= blen; 
        sent += blen;
    }
    // NRF_LOG_RAW_INFO("RX: ");
    // NRF_LOG_RAW_HEXDUMP_INFO(rdbuf, count);
    // NRF_LOG_RAW_INFO("\r\n");
}

static void spi_cs_assert(spi_interface *spi_if)
{
    struct spi_extra *spi_info = (struct spi_extra *)spi_if->extra;
    while( spi_info->hspi->State == HAL_SPI_STATE_BUSY );
    HAL_GPIO_WritePin(spi_info->ss_Port, spi_info->ss_Pin, GPIO_PIN_RESET);
}

static void spi_cs_release(spi_interface *spi_if)
{
    struct spi_extra *spi_info = (struct spi_extra *)spi_if->extra;
    while( spi_info->hspi->State == HAL_SPI_STATE_BUSY );
    HAL_GPIO_WritePin(spi_info->ss_Port, spi_info->ss_Pin, GPIO_PIN_SET);
}
