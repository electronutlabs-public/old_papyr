/** @file debug_serial.c
 *
 * @brief This file handles stdin/stdout
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/

#include "stm32f4xx_hal.h"
#include "main.h"
#include "usbd_cdc_if.h"

//  #define SERIAL_USE_DMA
#define SERIAL_USE_POLL
//#define SERIAL_USE_USB_CDC

extern DMA_HandleTypeDef hdma_usart1_tx;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

static volatile int tx_in_progress;

int _read(int file, char *buf, int len)
{
    UART_HandleTypeDef *huart;
    if(file == 1)
    {
        huart = &huart1;
    }
    else if(file == 3)
    {
        huart = &huart2;
    }
    
    int rcvd = 0;
    while(rcvd < len)
    {
        if(HAL_UART_Receive(huart, (uint8_t *)buf+rcvd, 1, 1000) == HAL_OK)
        {
            rcvd++;
        }
        else
        {
            break;
        }
    }

    return rcvd;
}

int _write(int file, char *buf, int len)
{
    if(file == 1)
    {
        #if defined(SERIAL_USE_POLL)
        HAL_UART_Transmit(&huart1, (uint8_t *)buf, len, 1000);
        #elif defined(SERIAL_USE_USB_CDC)
        CDC_Transmit_FS(buf, len);
        #elif defined(SERIAL_USE_DMA)
        HAL_StatusTypeDef status;

        tx_in_progress = 1;

        status = HAL_UART_Transmit_DMA(&huart1, (uint8_t *)buf, len);

        if (status != HAL_OK)
        {
            tx_in_progress = 0;
            len = -1;
        }
        while (tx_in_progress == 1);
        #endif

        return len;
    }
    else if(file == 3)
    {
        HAL_UART_Transmit(&huart2, (uint8_t *)buf, len, 1000);
        return len;
    }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    // this is from MA, check correct USART1/2
    tx_in_progress = 0;
}
