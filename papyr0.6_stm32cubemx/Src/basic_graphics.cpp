/** @file basic_graphics.cpp
 *
 * @brief Graphics drawing functions to test epaper
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/

#include <math.h>
#include <stm32f4xx.h>
#include <arm_math.h>
#include <FrameBuffer.h>
#include "basic_graphics.h"

static float dist(float x, float y, float x1, float y1) 
{
    return sqrt(powf(x-x1, 2)+powf(y-y1, 2));
}

void  mandelbrot(FrameBuffer *fb, int iterations, uint8_t color)
{
    // Establish a range of values on the complex plane
    // A different range will allow us to "zoom" in or out on the fractal
    int height = fb->height();
    int width = fb->width();
    
    // It all starts with the width, try higher or lower values
    float w = 4;
    float h = (w * height) / width;
    
    // Start at negative half the width and height
    float xmin = -w/2;
    float ymin = -h/2;
    
    // Maximum number of iterations for each point on the complex plane
    int maxiterations = iterations;
    
    // x goes from xmin to xmax
    float xmax = xmin + w;
    // y goes from ymin to ymax
    float ymax = ymin + h;
    
    // Calculate amount we increment x,y for each pixel
    float dx = (xmax - xmin) / (width);
    float dy = (ymax - ymin) / (height);
    
    // Start y
    float y = ymin;
    for (int j = 0; j < height; j++) {
        // Start x
        float x = xmin;
        for (int i = 0; i < width; i++) {
            
            // Now we test, as we iterate z = z^2 + cm does z tend towards infinity?
            float a = x;
            float b = y;
            int n = 0;
            while (n < maxiterations) {
                float aa = a * a;
                float bb = b * b;
                float twoab = 2.0 * a * b;
                a = aa - bb + x;
                b = twoab + y;
                // Infinty in our finite world is simple, let's just consider it 16
                if (dist(aa, bb, 0, 0) > 4.0) {
                    break;  // Bail
                }
                n++;
            }
            
            // We color each pixel based on how long it takes to get to infinity
            // If we never got there, let's pick the color black
            if (n == maxiterations) {
                //   pixels[i+j*width] = color(0);
                fb->putpixel(i, j, color);
            } 
            else {
                // Gosh, we could make fancy colors here if we wanted
                //   float norm = map(n, 0, maxiterations, 0, 1);
                //   pixels[i+j*width] = color(map(sqrt(norm), 0, 1, 0, 255));
            }
            x += dx;
        }
        y += dy;
    }
}


void circle(FrameBuffer *fb, int x0, int y0, int radius, uint8_t col)
{
    int x = radius-1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (radius << 1);
    
    while (x >= y)
    {
        fb->putpixel(x0 + x, y0 + y, col);
        fb->putpixel(x0 + y, y0 + x, col);
        fb->putpixel(x0 - y, y0 + x, col);
        fb->putpixel(x0 - x, y0 + y, col);
        fb->putpixel(x0 - x, y0 - y, col);
        fb->putpixel(x0 - y, y0 - x, col);
        fb->putpixel(x0 + y, y0 - x, col);
        fb->putpixel(x0 + x, y0 - y, col);
        
        if (err <= 0)
        {
            y++;
            err += dy;
            dy += 2;
        }
        if (err > 0)
        {
            x--;
            dx += 2;
            err += (-radius << 1) + dx;
        }
    }
}

void line(FrameBuffer *fb, int x0, int y0, int x1, int y1, uint8_t col) 
{
    
    int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
    int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
    int err = (dx>dy ? dx : -dy)/2, e2;
    
    for(;;)
    {
        fb->putpixel(x0,y0, col);
        if (x0==x1 && y0==y1) break;
        e2 = err;
        if (e2 >-dx) { err -= dy; x0 += sx; }
        if (e2 < dy) { err += dx; y0 += sy; }
    }
}
