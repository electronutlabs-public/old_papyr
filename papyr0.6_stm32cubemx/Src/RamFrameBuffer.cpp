/** @file RamFrameBuffer.cpp
 *
 * @brief This file contains the RamFrameBuffer class
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/
  
#include "RamFrameBuffer.h"
#include <string.h>

RamFrameBuffer::RamFrameBuffer(uint8_t *fb_addr, uint8_t t, uint16_t w, int16_t h)
:_addr(fb_addr),
_type(t),
_width(w),
_height(h)
{

}

// setters
void RamFrameBuffer::set_addr(uint8_t * addr)
{
    _addr = addr;
}

void RamFrameBuffer::width(uint16_t w)
{
    _width = w;
}

void RamFrameBuffer::height(uint16_t h)
{
    _height = h;
}

void RamFrameBuffer::type(uint8_t t)
{
    _type = t;
}

// getters
uint8_t RamFrameBuffer::bits_per_pixel()
{
    uint8_t bpp = 0;

    switch(_type)
    {
        case IMAGE_TYPE_MONOCHROME:
            bpp = 1;
            break;
        case IMAGE_TYPE_GS4:
            bpp = 2;
            break;
        case IMAGE_TYPE_MONOCHROME_RAW:
            bpp = 2;
            break;
        case IMAGE_TYPE_GS16:
            bpp = 4;
            break;
    }

    return bpp;
}

uint16_t RamFrameBuffer::width()
{
    return _width;
}

uint16_t RamFrameBuffer::height()
{
    return _height;
}

uint8_t RamFrameBuffer::type()
{
    return _type;
}

uint8_t* RamFrameBuffer::raw_ptr()
{
    return _addr;
}

uint32_t RamFrameBuffer::size()
{
    return _width * _height * bits_per_pixel() / 8;
}

void RamFrameBuffer::clear(uint8_t col)
{
    if(!_addr)
    {
        return;
    }

    uint8_t bpp = bits_per_pixel();
    uint8_t val = col&((1<<bpp)-1);
    for(int i=0; i<(8/bpp); i++)
    {
        val = val | (val<<bpp);
    }

    memset(_addr, val, size());
}
