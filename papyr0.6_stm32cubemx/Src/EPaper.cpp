/** @file EPaper.cpp
 *
 * @brief This file contains the EPaper class
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/
 
#include <stdint.h>
#include <string.h>
#include <stm32f4xx_hal.h>
#include <cmsis_os.h>
#include <EPaper.h>
#include "array60.h"
#include "TPS65185.h"
#include "BlockDevice.h"
#include "board.h"
#include "main.h"

// lookup tables
uint8_t wave_gs4_table[256][FRAME_GS4_LEN_60]; // stores values of all possible grayscale bytes to correct (18) frames of output bytes
uint8_t wave_gs4[FRAME_GS4_DEPTH][FRAME_GS4_LEN_60];

// static uint8_t wave_gs16[FRAME_GS16_DEPTH][FRAME_GS16_LEN_60];

uint8_t wave_monochrome_table[256][FRAME_MONOCHROME_LEN_60][2]; // stores values of all possible grayscale bytes to correct (18) frames of output bytes
uint8_t wave_monochrome[FRAME_MONOCHROME_DEPTH][FRAME_MONOCHROME_LEN_60];

// SPV = STV
// SCLK = CPV
#define E_SPV_GPIO_Port		E_STV_GPIO_Port
#define E_SPV_Pin	    	E_STV_Pin
#define E_SCLK_GPIO_Port	E_CLK_GPIO_Port
#define E_SCLK_Pin		    E_CLK_Pin

// #define SET_OUTPUT(x) gpio_mode_setup(x##_GPIO_Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, x)
//#define SET_OUTPUT(x)	do{_g.Pin=x##_Pin; _g.Mode=GPIO_MODE_OUTPUT_PP; HAL_GPIO_Init(x##_GPIO_Port, &_g);}while(0)
//#define SET_HIZ(x)		do{_g.Pin=x##_Pin; _g.Mode=GPIO_MODE_ANALOG; HAL_GPIO_Init(x##_GPIO_Port, &_g);}while(0)

#define SET_OUTPUT(x)

#define ACTUAL_FAST_HAL_GPIO_WritePin(g, p, s) \
  do{if(s != GPIO_PIN_RESET) \
    g->BSRR = p; \
  else \
    g->BSRR = (uint32_t)p << 16U; \
    asm("nop");asm("nop");asm("nop"); asm("nop");asm("nop"); \
    asm("nop");asm("nop");asm("nop"); asm("nop");asm("nop"); \
}while(0);


#define FAST_HAL_GPIO_WritePin(g, p, s)				ACTUAL_FAST_HAL_GPIO_WritePin(g, p, s)

// signals
#define LE_HIGH()               FAST_HAL_GPIO_WritePin(E_LE_GPIO_Port, E_LE_Pin, GPIO_PIN_SET)
#define LE_LOW()                FAST_HAL_GPIO_WritePin(E_LE_GPIO_Port, E_LE_Pin, GPIO_PIN_RESET)

#define OE_HIGH()               FAST_HAL_GPIO_WritePin(E_OE_GPIO_Port, E_OE_Pin, GPIO_PIN_SET)
#define OE_LOW()                FAST_HAL_GPIO_WritePin(E_OE_GPIO_Port, E_OE_Pin, GPIO_PIN_RESET)

#define SPH_HIGH()              FAST_HAL_GPIO_WritePin(E_SPH_GPIO_Port, E_SPH_Pin, GPIO_PIN_SET)
#define SPH_LOW()               FAST_HAL_GPIO_WritePin(E_SPH_GPIO_Port, E_SPH_Pin, GPIO_PIN_RESET)

#define SPV_HIGH()              FAST_HAL_GPIO_WritePin(E_SPV_GPIO_Port, E_SPV_Pin, GPIO_PIN_SET)
#define SPV_LOW()               FAST_HAL_GPIO_WritePin(E_SPV_GPIO_Port, E_SPV_Pin, GPIO_PIN_RESET)

#define SCLK_HIGH()             FAST_HAL_GPIO_WritePin(E_SCLK_GPIO_Port, E_SCLK_Pin, GPIO_PIN_SET)
#define SCLK_LOW()              FAST_HAL_GPIO_WritePin(E_SCLK_GPIO_Port, E_SCLK_Pin, GPIO_PIN_RESET)

#define CPV_HIGH()              FAST_HAL_GPIO_WritePin(E_CPV_GPIO_Port, E_CPV_Pin, GPIO_PIN_SET)
#define CPV_LOW()               FAST_HAL_GPIO_WritePin(E_CPV_GPIO_Port, E_CPV_Pin, GPIO_PIN_RESET)

#if defined(BOARD_DESTM32_L)
// voltage controls
#define VCOM_CTRL_HIGH()         FAST_HAL_GPIO_WritePin(VCOM_CTRL_GPIO_Port, VCOM_CTRL_Pin, GPIO_PIN_SET)
#define VCOM_CTRL_LOW()          FAST_HAL_GPIO_WritePin(VCOM_CTRL_GPIO_Port, VCOM_CTRL_Pin, GPIO_PIN_RESET)

#define VNEGGVEE_CTR_HIGH()     FAST_HAL_GPIO_WritePin(VNEGGVEE_CTR_GPIO_Port, VNEGGVEE_CTR_Pin, GPIO_PIN_SET)
#define VNEGGVEE_CTR_LOW()      FAST_HAL_GPIO_WritePin(VNEGGVEE_CTR_GPIO_Port, VNEGGVEE_CTR_Pin, GPIO_PIN_RESET)

#define VPOS15_CTR_HIGH()       FAST_HAL_GPIO_WritePin(VPOS15_CTR_GPIO_Port, VPOS15_CTR_Pin, GPIO_PIN_SET)
#define VPOS15_CTR_LOW()        FAST_HAL_GPIO_WritePin(VPOS15_CTR_GPIO_Port, VPOS15_CTR_Pin, GPIO_PIN_RESET)

#define GVDD22_CTR_HIGH()       FAST_HAL_GPIO_WritePin(GVDD22_CTR_GPIO_Port, GVDD22_CTR_Pin, GPIO_PIN_SET)
#define GVDD22_CTR_LOW()        FAST_HAL_GPIO_WritePin(GVDD22_CTR_GPIO_Port, GVDD22_CTR_Pin, GPIO_PIN_RESET)
#endif

#if defined(BOARD_PAPYR_V0_6)
#define WAKEUP_HIGH()			FAST_HAL_GPIO_WritePin(WAKEUP_GPIO_Port, WAKEUP_Pin, GPIO_PIN_SET)
#define WAKEUP_LOW()			FAST_HAL_GPIO_WritePin(WAKEUP_GPIO_Port, WAKEUP_Pin, GPIO_PIN_RESET)

#define PWRUP_HIGH()			FAST_HAL_GPIO_WritePin(PWRUP_GPIO_Port, PWRUP_Pin, GPIO_PIN_SET)
#define PWRUP_LOW()				FAST_HAL_GPIO_WritePin(PWRUP_GPIO_Port, PWRUP_Pin, GPIO_PIN_RESET)

#define VCOM_CTRL_HIGH()         FAST_HAL_GPIO_WritePin(VCOM_CTRL_GPIO_Port, VCOM_CTRL_Pin, GPIO_PIN_SET)
#define VCOM_CTRL_LOW()          FAST_HAL_GPIO_WritePin(VCOM_CTRL_GPIO_Port, VCOM_CTRL_Pin, GPIO_PIN_RESET)
#endif

// extras
#define E_GMODE1_HIGH()       FAST_HAL_GPIO_WritePin(E_GMODE1_GPIO_Port, E_GMODE1_Pin, GPIO_PIN_SET)
#define E_GMODE1_LOW()        FAST_HAL_GPIO_WritePin(E_GMODE1_GPIO_Port, E_GMODE1_Pin, GPIO_PIN_RESET)

#define E_GMODE2_HIGH()       FAST_HAL_GPIO_WritePin(E_GMODE2_GPIO_Port, E_GMODE2_Pin, GPIO_PIN_SET)
#define E_GMODE2_LOW()        FAST_HAL_GPIO_WritePin(E_GMODE2_GPIO_Port, E_GMODE2_Pin, GPIO_PIN_RESET)

#define SET_DATA_PINS(x)        do{E_DB_ODR = ((E_DB_ODR & E_DB_ODR_MASK) | (x & 0xff));}while(0)

#if 1
static inline void delay_cycle(volatile unsigned long x)
{
    while (x--)
    {
        asm("nop");
    }
}
#else
static void delay_cycle(volatile unsigned long nCount)
{
    for(; nCount != 0; nCount--);
}
#endif

// mode can be GPIO_MODE_OUTPUT_PP, or GPIO_MODE_ANALOG etc.
static inline void set_pins_mode(uint32_t mode)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = 0;
    GPIO_InitStruct.Mode = mode;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    

    // init variables
    GPIO_InitStruct.Pin = E_LE_Pin; HAL_GPIO_Init(E_LE_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_OE_Pin; HAL_GPIO_Init(E_OE_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_SPH_Pin; HAL_GPIO_Init(E_SPH_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_SPV_Pin; HAL_GPIO_Init(E_SPV_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_SCLK_Pin; HAL_GPIO_Init(E_SCLK_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_CPV_Pin; HAL_GPIO_Init(E_CPV_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_GMODE1_Pin; HAL_GPIO_Init(E_GMODE1_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_GMODE2_Pin; HAL_GPIO_Init(E_GMODE2_GPIO_Port, &GPIO_InitStruct);
    #if defined(BOARD_DESTM32_L)
    GPIO_InitStruct.Pin = VCOM_CTRL_Pin; HAL_GPIO_Init(VCOM_CTRL_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = VNEGGVEE_CTR_Pin; HAL_GPIO_Init(VNEGGVEE_CTR_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = VPOS15_CTR_Pin; HAL_GPIO_Init(VPOS15_CTR_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GVDD22_CTR_Pin; HAL_GPIO_Init(GVDD22_CTR_GPIO_Port, &GPIO_InitStruct);
    #endif

	#if defined(BOARD_PAPYR_V0_6)
    GPIO_InitStruct.Pin = VCOM_CTRL_Pin; HAL_GPIO_Init(VCOM_CTRL_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = WAKEUP_Pin; HAL_GPIO_Init(WAKEUP_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = PWRUP_Pin; HAL_GPIO_Init(PWRUP_GPIO_Port, &GPIO_InitStruct);
	#endif

    GPIO_InitStruct.Pin = E_D0_Pin; HAL_GPIO_Init(E_D0_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_D1_Pin; HAL_GPIO_Init(E_D1_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_D2_Pin; HAL_GPIO_Init(E_D2_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_D3_Pin; HAL_GPIO_Init(E_D3_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_D4_Pin; HAL_GPIO_Init(E_D4_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_D5_Pin; HAL_GPIO_Init(E_D5_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_D6_Pin; HAL_GPIO_Init(E_D6_GPIO_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = E_D7_Pin; HAL_GPIO_Init(E_D7_GPIO_Port, &GPIO_InitStruct);
}

EPaper::EPaper()
:epaper_row_pixels(800)
,epaper_rows(600)
,fb(NULL)
#if defined(HAS_TPS65185)
,_tps65185(TPS65185(&I2C_HANDLE_T))
#endif
{
    #if defined(HAS_TPS65185)
    _tps65185.init();
    #endif
}

// power on sequence
void EPaper::power_on()
{
    set_pins_mode(GPIO_MODE_OUTPUT_PP);

    #if defined(BOARD_DESTM32_L)
    VNEGGVEE_CTR_HIGH();
    delay_cycle(0xf);
    VPOS15_CTR_HIGH();
    delay_cycle(0xf);
    GVDD22_CTR_HIGH();
    delay_cycle(0xf);

    VCOM_CTR_HIGH();                //VCOM = 0
    delay_cycle(0xff);
    #endif

	#if defined(BOARD_PAPYR_V0_6)
    VCOM_CTRL_HIGH();
    WAKEUP_HIGH();
    PWRUP_HIGH();
	#endif

    #if defined(HAS_TPS65185)
    _tps65185.makeActive();
    #endif

    delay_cycle(0xffffff);
}

// power off sequence
void EPaper::power_off()
{
    #if defined(BOARD_DESTM32_L)
    VCOM_CTR_LOW();             //VCOM = 0
    delay_cycle(0x2ff);

    GVDD22_CTR_LOW();
    delay_cycle(0x2f);
    VPOS15_CTR_LOW();
    delay_cycle(0x2f);
    VNEGGVEE_CTR_LOW();
    delay_cycle(0x2ff);
    #endif

	#if defined(BOARD_PAPYR_V0_6)
    VCOM_CTRL_LOW();
    WAKEUP_LOW();
    PWRUP_LOW();
	#endif

    #if defined(HAS_TPS65185)
    _tps65185.makeStandby();
    #endif

    set_pins_mode(GPIO_MODE_ANALOG);
}


// initialise display
void EPaper::init()
{
    // set up parameters
    uint8_t (*pr_gs4_68)[FRAME_GS4_LEN_60];

    pr_gs4_68 = wave_gs4_60;

    int i, j;

    for(i=0;i<4;i++)
    {
        for(j=0;j<FRAME_GS4_LEN_60;j++)
        {
            wave_gs4[i][j] = *(pr_gs4_68[i]+j);
        }
    }

    uint8_t (*pr_monochrome_68)[FRAME_MONOCHROME_LEN_60];
    pr_monochrome_68 = wave_monochrome_60;

    for(i=0;i<FRAME_MONOCHROME_DEPTH;i++)
	{
		for(j=0;j<FRAME_MONOCHROME_LEN_60;j++)
		{
			wave_monochrome[i][j] = *(pr_monochrome_68[i]+j);
		}
	}


    E_GMODE1_HIGH();
    E_GMODE2_HIGH();
    #if defined(BOARD_DESTM32_L)
    VCOM_CTR_LOW();                 //VCOM = 0
    GVDD22_CTR_LOW();
    VPOS15_CTR_LOW();
    VNEGGVEE_CTR_LOW();
    #endif

    set_pins_mode(GPIO_MODE_OUTPUT_PP);

    SET_DATA_PINS(0x00);

    power_off();

    LE_LOW();
    SCLK_LOW();
    OE_LOW();
    SPH_HIGH();
    SPV_HIGH();
    CPV_LOW();

    // create wave table
    _make_wave_table();
}

void EPaper::attach_fb(FrameBuffer *_fb)
{
    fb = _fb;
}

void EPaper::draw_fb()
{
    if(!fb)
    {
        return;
    }

    uint8_t *ptr = fb->raw_ptr();

    switch(fb->type())
    {
        case IMAGE_TYPE_MONOCHROME:
            show_image_monochrome(ptr);
            break;
        case IMAGE_TYPE_MONOCHROME_RAW:
            show_image_monochrome_raw(ptr);
            break;
        case IMAGE_TYPE_GS4:
            show_image_gs4(ptr);
            break;
    }
}

void EPaper::pin_test()
{
    power_off();

    again:
    SCLK_HIGH();
    OE_HIGH();
    LE_HIGH();
    SPV_HIGH();
    CPV_HIGH();
    SPH_HIGH();
    E_GMODE1_HIGH();
    E_GMODE2_HIGH();
    SET_DATA_PINS(0xff);
    osDelay(2000);
    SCLK_LOW();
    OE_LOW();
    LE_LOW();
    SPV_LOW();
    CPV_LOW();
    SPH_LOW();
    E_GMODE1_LOW();
    E_GMODE2_LOW();
    SET_DATA_PINS(0x00);
    osDelay(2000);
    goto again;

}

// 'wave tables'
void EPaper::_make_wave_table(void)
{
    int frame, num;
    unsigned char tmp,value;

    //wave_gs4_table
    for(frame=0; frame<FRAME_GS4_LEN_60; frame++)
    {
        for(num=0; num<256; num++)
        {
            tmp = 0;
            tmp = wave_gs4[(num)&0x3][frame];

            tmp = tmp<< 2;
            tmp &= 0xfffc;
            tmp |= wave_gs4[(num>>2)&0x3][frame];

            tmp = tmp<< 2;
            tmp &= 0xfffc;
            tmp |= wave_gs4[(num>>4)&0x3][frame];

            tmp = tmp<< 2;
            tmp &= 0xfffc;
            tmp |= wave_gs4[(num>>6)&0x3][frame];

            // TODO fix saved image
            value = tmp;
            // value = 0;
            // value = (tmp <<6) & 0xc0;
            // value += (tmp<<2) & 0x30;
            // value += (tmp>>2) & 0x0c;
            // value += (tmp>>6) & 0x03;
            wave_gs4_table[num][frame] = value;
        }
    }

    for(frame=0; frame<FRAME_MONOCHROME_LEN_60; frame++)
	{
		for(num=0; num<256; num++)
		{
			tmp = 0;
			tmp = wave_monochrome[(num)&0x1][frame];

			tmp = tmp<< 2;
			tmp &= 0xfffc;
			tmp |= wave_monochrome[(num>>1)&0x1][frame];

			tmp = tmp<< 2;
			tmp &= 0xfffc;
			tmp |= wave_monochrome[(num>>2)&0x1][frame];

			tmp = tmp<< 2;
			tmp &= 0xfffc;
			tmp |= wave_monochrome[(num>>3)&0x1][frame];

			wave_monochrome_table[num][frame][0] = tmp;

			tmp = 0;
			tmp = wave_monochrome[(num>>4)&0x1][frame];

			tmp = tmp<< 2;
			tmp &= 0xfffc;
			tmp |= wave_monochrome[(num>>5)&0x1][frame];

			tmp = tmp<< 2;
			tmp &= 0xfffc;
			tmp |= wave_monochrome[(num>>6)&0x1][frame];

			tmp = tmp<< 2;
			tmp &= 0xfffc;
			tmp |= wave_monochrome[(num>>7)&0x1][frame];

			wave_monochrome_table[num][frame][1] = tmp;
		}
	}
}

void EPaper::_map_frame_monochrome_rowdata(const uint8_t *new_pic, uint8_t frame)
{
    int i,n;

    n = epaper_row_pixels/8;

    for(i=0; i<n; i++)
    {
        // 1 byte of framebuffer converts to 2 bytes of row_data
        g_dest_data[i*2] = wave_monochrome_table[new_pic[i]][frame][0];
        g_dest_data[i*2+1] = wave_monochrome_table[new_pic[i]][frame][1];
    }
}

void EPaper::_map_frame_gs4_rowdata(const uint8_t *new_pic, uint8_t frame)
{
    int i,n;

    n = epaper_row_pixels/4;

    for(i=0; i<n; i++)
    {
        // one byte of frambuffer convets to 1 byte of row_data
        g_dest_data[i] = wave_gs4_table[new_pic[i]][frame];
    }
}

void EPaper::_vclock_quick()
{
    CPV_LOW();
    CPV_HIGH();
    CPV_LOW();
    CPV_HIGH();
}

void EPaper::_start_scan()
{
    SPV_HIGH();
    CPV_LOW();

    _vclock_quick();

    SPV_LOW();

    _vclock_quick();

    SPV_HIGH();

    _vclock_quick();
}

void EPaper::_send_row_data(const unsigned char *pArray, unsigned int disp_delay)
{
    unsigned int n;

    uint16_t timingA = disp_delay;
    uint16_t timingB = disp_delay/10;

    n = epaper_row_pixels/4;

    LE_HIGH();
    SCLK_LOW();
    SCLK_HIGH();
    SCLK_LOW();
    SCLK_HIGH();

    LE_LOW();
    SCLK_LOW();
    SCLK_HIGH();
    SCLK_LOW();
    SCLK_HIGH();

    OE_HIGH();
    SCLK_LOW();
    SCLK_HIGH();
    SCLK_LOW();
    SCLK_HIGH();
    SPH_LOW();

    for(unsigned int column=0; column < n; column++)
    {
        uint8_t new_data = pArray[column];

        SET_DATA_PINS(new_data);

        SCLK_LOW();
        delay_cycle(timingA);

        SCLK_HIGH();
        delay_cycle(timingB);

    }

    SPH_HIGH();
    SCLK_LOW();
    SCLK_HIGH();
    SCLK_LOW();
    SCLK_HIGH();

    delay_cycle(disp_delay);

    CPV_LOW();
    OE_LOW();
    SCLK_LOW();
    SCLK_HIGH();
    SCLK_LOW();
    SCLK_HIGH();

    delay_cycle(disp_delay);

    CPV_HIGH();

}

void EPaper::clear()
{
    //  const uint8_t clear_pattern[] = {
    //  		0x55,0xaa,0x55,0xaa,
    //  };

    static uint8_t black[200];
 	static uint8_t white[200];

    memset(white, WHITE_SEQ, sizeof(white));
    memset(black, BLACK_SEQ, sizeof(black));

    power_on();

#if 0

    for(unsigned int cycle=0; cycle<sizeof(clear_pattern); cycle++)
    {
        memset(g_dest_data, clear_pattern[cycle], sizeof(g_dest_data));

        for(int i=0;i<8;i++)
        {
            _start_scan();
            for(unsigned int line=0; line<epaper_rows; line++)
            {
                _send_row_data(g_dest_data);
            }
        }
    }

    memset(g_dest_data, 0, sizeof(g_dest_data));

	for(int i=0;i<5;i++)
	{
		_start_scan();
		for(unsigned int line=0; line<epaper_rows; line++)
		{
			_send_row_data(g_dest_data);
		}
	}
#endif

	const int clear_iter = 50;
	for(unsigned int cycle=0; cycle<clear_iter; cycle++)
	{
		// for(int i=0;i<clear_iter;i++)
		{
			_start_scan();
			for(unsigned int line=0; line<epaper_rows; line++)
			{
				_send_row_data(white, 0);
			}
		}

		// for(int i=0;i<clear_iter;i++)
		{
			_start_scan();
			for(unsigned int line=0; line<epaper_rows; line++)
			{
				_send_row_data(black, 0);
			}
		}

	}

	for(int i=0;i<4;i++)
	{
		_start_scan();
		for(unsigned int line=0; line<epaper_rows; line++)
		{
			_send_row_data(black);
		}
	}
	for(int i=0;i<4;i++)
	{
		_start_scan();
		for(unsigned int line=0; line<epaper_rows; line++)
		{
			_send_row_data(white);
		}
	}

    power_off();
}

void EPaper::show_block_device_image(BlockDevice &bd, uint64_t address, uint8_t type)
{
    int repeat = 8;
    int n = epaper_row_pixels/4;
    power_on();

    switch(type)
    {
        case IMAGE_TYPE_GS4:
            repeat = FRAME_GS4_LEN_60;
            break;
        case IMAGE_TYPE_MONOCHROME_RAW:
            repeat = FRAME_MONOCHROME_RAW_REPEAT;
            break;
        case IMAGE_TYPE_MONOCHROME:
        	n = epaper_row_pixels/4; // 8 pixels per byte
        	repeat = FRAME_MONOCHROME_LEN_60;
            break;
    }

    for(int frame=0; frame<repeat; frame++)
    {
        _start_scan();
        for(unsigned int line=0; line<epaper_rows; line++)
        {
            static uint8_t buf[200];// TODO this should be big enough for minimum depth
            bd.read(buf, address+line*n, n);
            switch(type)
            {
                case IMAGE_TYPE_MONOCHROME_RAW:
                    _send_row_data(buf);
                    break;
                case IMAGE_TYPE_MONOCHROME:
                	_map_frame_monochrome_rowdata(buf, frame);
					_send_row_data(g_dest_data);
                    break;
                case IMAGE_TYPE_GS4:
                    _map_frame_gs4_rowdata(buf, frame);
                    _send_row_data(g_dest_data);
                    break;
            }
        }
    }
    power_off();
}

void EPaper::show_image_monochrome(const uint8_t *ptr)
{
    int n = epaper_row_pixels/8; // 8 becuse 8 pixes per byte
    power_on();
    // todo fix last frame vales are wrong
	for(int frame=0; frame<FRAME_MONOCHROME_LEN_60; frame++)
    {
        _start_scan();
        for(unsigned int line=0; line<epaper_rows; line++)
        {
             _map_frame_monochrome_rowdata(ptr + line*n, frame);
            _send_row_data(g_dest_data);

        }
        _send_row_data( g_dest_data );
    }
    power_off();
}

void EPaper::show_image_monochrome_raw(const uint8_t *ptr)
{
    const int repeat = FRAME_MONOCHROME_RAW_REPEAT;
    int n = epaper_row_pixels/4;
    power_on();
    for(int frame=0; frame<repeat; frame++)
    {
        _start_scan();
        for(unsigned int line=0; line<epaper_rows; line++)
        {
            _send_row_data( ptr + line*n );

        }
    }
    power_off();
}

void EPaper::show_image_gs4(const uint8_t *ptr)
{
    int n = epaper_row_pixels/4;
    power_on();
    for(int frame=0; frame<FRAME_GS4_LEN_60; frame++)
    {
        _start_scan();
        for(unsigned int line=0; line<epaper_rows; line++)
        {
            _map_frame_gs4_rowdata(ptr + line*n, frame);
            _send_row_data(g_dest_data);
        }
    }
    power_off();
}
