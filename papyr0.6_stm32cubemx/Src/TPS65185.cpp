/** @file TPS65185.cpp
 *
 * @brief Class for epaper power managemnt chip
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/
 
#include <stm32f4xx_hal.h>
#include "TPS65185.h"

TPS65185::TPS65185(I2C_HandleTypeDef *hi2c)
:_hi2c(hi2c)
{

}

// initialise
void TPS65185::init()
{
    // set VCOM voltage
    _setVCOMVoltage(-2000);
}

// put in ACTIVE mode
void TPS65185::makeActive()
{
    uint8_t val = _readRegister(TPS65185_ENABLE);
    // set ENABLE bit
    val = val | (1 << 7);
    // write 
    _writeRegister(TPS65185_ENABLE, val);
}

// put in STANDBY mode
void TPS65185::makeStandby()
{
    uint8_t val = _readRegister(TPS65185_ENABLE);
    // set STANDBY bit 
    val = val | (1 << 6);
    // write
    _writeRegister(TPS65185_ENABLE, val);
}


// read register
uint8_t TPS65185::_readRegister(uint8_t reg)
{
    uint8_t val = 0;

    HAL_I2C_Mem_Read(_hi2c, TPS65185_I2C_ADDR, reg, I2C_MEMADD_SIZE_8BIT, &val, 1, 10000);

    return val;
}

// write to register
void TPS65185::_writeRegister(uint8_t reg, uint8_t value)
{
    HAL_I2C_Mem_Write(_hi2c, TPS65185_I2C_ADDR, reg, I2C_MEMADD_SIZE_8BIT, &value, 1, 10000);
}
    
// set VCOM voltage (negative, mV) in VCOM1
/*
    From datasheet:

    VCOM voltage adjustment
    VCOM = VCOM[8:0] x –10 mV in the range from 0 mV to –5.110 V 0h = –0 mV
    1h = –10 mV
    2h = –20 mV
    ...
    7Dh = –1250 mV
    ...
    1FEh = –5100 mV
    1FFh = –5110 mV
*/
void TPS65185::_setVCOMVoltage(int voltageMV)
{

    // valid range? Only 0 to -5110 mV
    if(voltageMV > 0  || voltageMV < -5110) {
        return;
    }

    // calculate VCOM[8:0] register value
    uint16_t regVal = -voltageMV/10;

    // set VCOM[7:0]
    uint8_t val1 = regVal & 0xFF;
    _writeRegister(TPS65185_VCOM1, val1);

    // set VCOM[8]:

    // read first
    uint8_t val2 = _readRegister(TPS65185_VCOM2);
    // set bit 
    val2 &= 0xFE;
	val2 |= (regVal >> 8) & 0x01;

    // write
    _writeRegister(TPS65185_VCOM2, val2);

    uint8_t val3 = _readRegister(TPS65185_VCOM1);

    uint8_t val4 = _readRegister(TPS65185_VCOM2);
}
