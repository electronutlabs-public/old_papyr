#include "sys.h" 
#include "text.h"	
#include "string.h"												    
#include "usart.h"		
#include "MyLib_w32x16.h"
#include "MyLib_w32x16aux.h"
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//汉字显示 驱动代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2014/5/15
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved									  
////////////////////////////////////////////////////////////////////////////////// 	 


u8 dzk[520];
//unsigned char font_test[20]={0xCE,0xD2,0xc3,0xc7,0xca,0xc7,0xb9,0xb2,0xb2,0xfa,0xd6,0xf7,0xd2,0xe5,0xbd,0xd3,0xb0,0xe0,0xc8,0xcb};
unsigned char font_test[20]={0xB0,0xA1,0xB0,0xAC,0xca,0xc7,0xb9,0xb2,0xb2,0xfa,0xd6,0xf7,0xd2,0xe5,0xbd,0xd3,0xb0,0xe0,0xc8,0xcb};
//unsigned char font_test[20]={0xC1,0xf5,0xB0,0xAC,0xC3,0xF7,0xb9,0xb2,0xb2,0xfa,0xd6,0xf7,0xd2,0xe5,0xbd,0xd3,0xb0,0xe0,0xc8,0xcb};
	
//code 字符指针开始
//从字库中查找出字模
//code 字符串的开始地址,GBK码
//mat  数据存放地址 (size/8+((size%8)?1:0))*(size) bytes大小	
//size:字体大小

void Get_HzMat(unsigned char *code,unsigned char *mat,u8 size)
{		    
	unsigned char qh,ql;
	unsigned char i;					  
	unsigned long foffset; 
	u16 csize=(size/8+((size%8)?1:0))*(size);//得到字体一个字符对应点阵集所占的字节数	 
	qh=*code;
	ql=*(++code);
	if(qh<0x81||ql<0x40||ql==0xff||qh==0xff)//非 常用汉字
	{   		    
	    for(i=0;i<csize;i++)*mat++=0x00;//填充满格
	    return; //结束访问
	}          
	if(ql<0x7f)ql-=0x40;//注意!
	else ql-=0x41;
	qh-=0x81;   
	foffset=((unsigned long)190*qh+ql)*csize;	//得到字库中的字节偏移量  		  
	switch(size)
	{
		case 64:
			MyLib_W32X_Flash_Byte_Read(W32X16,Font_Adrr+foffset,mat,csize);
			break;
		case 16:
			MyLib_W32X_Flash_Byte_Read(W32X16,Font_Adrr+foffset,mat,csize);
			break;
		case 24:
			MyLib_W32X_Flash_Byte_Read(W32X16,Font_Adrr+foffset,mat,csize);
			break;
		case 32:
			MyLib_W32X_Flash_Byte_Read(W32X16,Font_Adrr+foffset,mat,csize);
			break;
			
	}     												    
}  
//显示一个指定大小的汉字
//x,y :汉字的坐标
//font:汉字GBK码
//size:字体大小
//mode:0,正常显示,1,叠加显示	
 
 
void Show_Font(u8 *font,u8 size,u8 mode)
{
	if(size!=64&&size!=16&&size!=24&&size!=32)		return;	//不支持的size
	
	Get_HzMat(font,dzk,size);	//得到相应大小的点阵数据
	
}
	
	



























		  






