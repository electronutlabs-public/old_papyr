
/**
  ******************************************************************************
  * @file    Project/STM32F4xx_StdPeriph_Templates/stm32f4xx_it.c 
  * @author  MCD Application Team
  * @version V1.4.0
  * @date    04-August-2014
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"
#include "usart2.h"
#include "usart3.h"
#include "MyLib_w32x16.h"
#include "key.h"
 

/** @addtogroup Template_Project
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
 
}


void TIM3_IRQHandler(void)
{
/*	if(TIM_GetITStatus(TIM3,TIM_IT_Update)==SET) //溢出中断
	{
		if(USART1_Start == 0x55)
		{
			if(USART1_ScanDelay != 0)
			{
				USART1_ScanDelay--;
				
				if(USART1_ScanDelay == 0)
					USART1_ScanDelay_Flag = 0x55;
			}			
		}

		if(USART2_Start == 0x55)
			{
				if(USART2_ScanDelay != 0)
				{
					USART2_ScanDelay--;
					
					if(USART2_ScanDelay == 0)
						USART2_ScanDelay_Flag = 0x55;
				}			
			}
			
			if(USART3_Start == 0x55)
			{
				if(USART3_ScanDelay != 0)
				{
					USART3_ScanDelay--;
					
					if(USART3_ScanDelay == 0)
						USART3_ScanDelay_Flag = 0x55;
				}			
			}
			
			if(UART4_Start == 0x55)
			{
				if(UART4_ScanDelay != 0)
				{
					UART4_ScanDelay--;
					
					if(UART4_ScanDelay == 0)
						UART4_ScanDelay_Flag = 0x55;
				}			
			}

			if(Flash_Erase_Flag == 0x55)				//Flash整片擦除需要200s
			{
				if(Flash_Erase_Delay != 0)
				{
					Flash_Erase_Delay--;
					if(Flash_Erase_Delay < 100)
					{
						Flash_Erase_Flag = 0xaa;
						Flash_Erase_Delay = 0;
					}
				}					
			}

			if((GPIOD->IDR & GPIO_Pin_7) == (uint32_t)Bit_RESET)
			{				
				if(key1_delay < 6)		key1_delay++;									
			}
			else
			{
				if(key1_delay >=4 )
				{
					key1_delay = 0;
					keyinput_flag |= k1_flag;				
				}			
			}
			
			if((GPIOE->IDR & GPIO_Pin_2) == (uint32_t)Bit_RESET)
			{				
				if(key2_delay < 6)		key2_delay++;									
			}
			else
			{
				if(key2_delay >=4 )
				{
					key2_delay = 0;
					keyinput_flag |= k2_flag;				
				}			
			}
			
			if((GPIOA->IDR & GPIO_Pin_8) == (uint32_t)Bit_RESET)
			{				
				if(key3_delay < 6)		key3_delay++;									
			}
			else
			{
				if(key3_delay >=4 )
				{
					key3_delay = 0;
					keyinput_flag |= k3_flag;				
				}			
			}
			
			if((GPIOA->IDR & GPIO_Pin_11) == (uint32_t)Bit_RESET)
			{				
				if(key4_delay < 6)		key4_delay++;									
			}
			else
			{
				if(key4_delay >=4 )
				{
					key4_delay = 0;
					keyinput_flag |= k4_flag;				
				}			
			}
			
//			if((GPIOG->IDR & GPIO_Pin_8) != (uint32_t)Bit_RESET)
//			{				
//				if(start_flag != 0x55)
//				{
//					if(bat_check_delay < 6)		
//						bat_check_delay++;	
//				}
//			}
//			else
//			{
//				start_flag = 0;
//				
//				if(bat_check_delay >=4 )
//				{
//					bat_check_delay = 0;
//					bat_check_flag = 0x55;		
//				}			
//			}
			if((GPIOG->IDR & GPIO_Pin_8) != (uint32_t)Bit_RESET)
			{				
					if(bat_check_delay < 6)		
						bat_check_delay++;	
			}
			else
			{
				
				if(bat_check_delay >=4 )
				{
					bat_check_delay = 0;
					bat_check_flag = 0x55;		
				}			
			}
	}*/
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);  //清除中断标志位
}
/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
