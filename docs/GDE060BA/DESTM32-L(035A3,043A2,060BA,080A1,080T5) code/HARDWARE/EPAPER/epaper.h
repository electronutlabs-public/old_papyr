#ifndef __EPAPER_H
#define __EPAPER_H		
#include <stm32f4xx.h>


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//EPD 接口定义
#define EPD_CL_PORT					GPIOE
#define EPD_CL         			GPIO_Pin_5		//PE5			CL		 WR
#define EPD_OE_PORT					GPIOE
#define EPD_OE            	GPIO_Pin_4		//PE4			OE     OE
#define EPD_LE_PORT					GPIOE
#define EPD_LE            	GPIO_Pin_6		//PE6   	LE     LE
//#define EPD_SHR_PORT				GPIOC
//#define EPD_SHR             GPIO_Pin_13		//PC13			SHR   
#define EPD_SHR_PORT				GPIOB
#define EPD_SHR             GPIO_Pin_7		//PB7			SHR  
#define EPD_GMODE1_PORT     GPIOF
#define EPD_GMODE1          GPIO_Pin_7		//PF7				MODE1
#define EPD_GMODE2_PORT     GPIOF
#define EPD_GMODE2          GPIO_Pin_10		//PF10				MODE2
#define EPD_XRL_PORT				GPIOF
#define EPD_XRL             GPIO_Pin_9		//PF9				L/R

#define EPD_DB_PORT         GPIOC
#define EPD_DB_0            GPIO_Pin_0		//PC0			DATA
#define EPD_DB_1            GPIO_Pin_1		//PC1
#define EPD_DB_2            GPIO_Pin_2		//PC2
#define EPD_DB_3            GPIO_Pin_3		//PC3
#define EPD_DB_4            GPIO_Pin_4		//PC4
#define EPD_DB_5            GPIO_Pin_5		//PC5
#define EPD_DB_6            GPIO_Pin_6		//PC6
#define EPD_DB_7            GPIO_Pin_7		//PC7

#define EPD_XSPV_PORT				GPIOF
#define EPD_XSPV            GPIO_Pin_6		//PF6		SPV
#define EPD_CLK_PORT				GPIOF
#define EPD_CLK             GPIO_Pin_8		//PF8		CPV
#define EPD_SPH_PORT				GPIOE
#define EPD_SPH             GPIO_Pin_3		//PE3		SPH

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//EPD 电源 控制定义
#define VCOM_CTR_PORT      		GPIOG	
#define VCOM_CTR           		GPIO_Pin_12		//PG12			4053 S2
#define ASW1_CTR_PORT      		GPIOG	
#define ASW1_CTR           		GPIO_Pin_11		//PG11			4053	S1
#define ASW3_CTR_PORT      		GPIOG	
#define ASW3_CTR           		GPIO_Pin_13		//PG13			4053	S3

#define VNEGGVEE_CTR_PORT			GPIOB
#define VNEGGVEE_CTR					GPIO_Pin_9		//PB9			AME5142
#define VPOS15_CTR_PORT				GPIOG
#define VPOS15_CTR						GPIO_Pin_14		//PG14		+15V
#define GVDD22_CTR_PORT				GPIOB
#define GVDD22_CTR						GPIO_Pin_8		//PB8			+22V


#define TEST_CTR_PORT       	GPIOC
#define	TEST_CTR							GPIO_Pin_3		//PC3

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	
#define PORT_B_PIN_OUT  			GVDD22_CTR | VNEGGVEE_CTR | EPD_SHR
#define PORT_C_PIN_OUT  			EPD_DB_0 | EPD_DB_1 | EPD_DB_2 | EPD_DB_3 | EPD_DB_4 | EPD_DB_5 | EPD_DB_6 | EPD_DB_7
#define PORT_E_PIN_OUT				EPD_CL | EPD_OE | EPD_LE | EPD_SPH
#define PORT_F_PIN_OUT  	    EPD_GMODE1 | EPD_GMODE2 | EPD_XRL | EPD_XSPV | EPD_CLK 
#define PORT_G_PIN_OUT  	    VCOM_CTR | ASW1_CTR | ASW3_CTR | VPOS15_CTR

#define PORT_C_PIN_IN 				TEST_CTR
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#define RCC_GPIO_USE        	RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOE | RCC_AHB1Periph_GPIOF | RCC_AHB1Periph_GPIOG
#define GPIO_SPEED_EPD      	GPIO_Speed_50MHz//GPIO_Speed_100MHz



//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//EPD端口宏定义 
#define EPD_CL_H            EPD_CL_PORT->BSRRL =EPD_CL
#define EPD_CL_L            EPD_CL_PORT->BSRRH =EPD_CL

#define EPD_LE_H            EPD_LE_PORT->BSRRL =EPD_LE
#define EPD_LE_L            EPD_LE_PORT->BSRRH =EPD_LE

#define EPD_OE_H            EPD_OE_PORT->BSRRL =EPD_OE
#define EPD_OE_L            EPD_OE_PORT->BSRRH =EPD_OE

#define EPD_SPH_H           EPD_SPH_PORT->BSRRL =EPD_SPH
#define EPD_SPH_L           EPD_SPH_PORT->BSRRH =EPD_SPH

#define EPD_XSPV_H          EPD_XSPV_PORT->BSRRL =EPD_XSPV
#define EPD_XSPV_L          EPD_XSPV_PORT->BSRRH =EPD_XSPV

#define EPD_CLK_H           EPD_CLK_PORT->BSRRL =EPD_CLK
#define EPD_CLK_L           EPD_CLK_PORT->BSRRH =EPD_CLK

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx




//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
extern unsigned long item_dis;
extern unsigned char u_test;
extern unsigned int epaper_line,epaper_row;
extern unsigned char FRAME_BEGIN_LEN;
extern unsigned char FRAME_END_LEN;
extern unsigned char FRAME_INIT_LEN;
extern unsigned char wave_begin[4][30];
extern unsigned char wave_end[4][30];
extern unsigned long wave_init[30];


//函数声明
void Delay(vu32 nCount);
void Epaper_Init(void);

void make_wave_table(void);

void EPD_PowerOn(void);
void EPD_PowerOff(void);
void EPD_Init(void);
void EPD_Start_Scan(void);
void EPD_Send_Row_Data(unsigned char  *pArray);
void EPD_Display_PIC(void);

/***************************4.3*********************************/
extern unsigned char gImage_lanse_43[120000];

extern unsigned char gImage_yinxin_43[120000];

extern unsigned char acgs_43[120000];

/***************************3.5*********************************/
extern unsigned char gImage_shangxia[96000];

extern unsigned char gImage_jiyu[96000];


/***************************6.0*********************************/
//extern unsigned char gImage_lanse_60[120000];

//extern unsigned char gImage_yinxin_60[120000];

//extern unsigned char acgs_60[120000] ;


/****************************8.0***********************************/
extern unsigned char ac_80[196608];

extern unsigned char gImage_test_80[196608];

extern unsigned char acgs_80[196608]; 


#endif
