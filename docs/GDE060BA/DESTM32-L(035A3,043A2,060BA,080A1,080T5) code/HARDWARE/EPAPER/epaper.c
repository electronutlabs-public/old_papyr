#include "usart.h"	 
#include "delay.h"	
#include "epaper.h"	
#include "stm32f4xx.h"
#include "key.h"



//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//驱动波形
/******************************************新波形文件****************************************/
/*
//旧图片，刷到白
#if 0
#define FRAME_BEGIN_LEN		18
unsigned char wave_begin[4][FRAME_BEGIN_LEN]=
{
0,0,0,0,0,2,2,2,2,1,1,1,1,2,2,2,2,0,						//GC0->GC3
0,0,0,1,1,2,2,2,2,1,1,1,1,2,2,2,2,0,						//GC1->GC3
0,0,1,1,1,2,2,2,2,1,1,1,1,2,2,2,2,0,						//GC2->GC3
0,1,1,1,1,2,2,2,2,1,1,1,1,2,2,2,2,0,						//GC3->GC3
};
#else
#define FRAME_BEGIN_LEN		18
unsigned char wave_begin[4][FRAME_BEGIN_LEN]=
{
0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,0,						//GC0->GC3
0,0,0,0,1,1,1,1,1,2,2,2,2,2,2,2,2,0,						//GC1->GC3
0,0,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,0,						//GC2->GC3
0,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,0,						//GC3->GC3
};
#endif

//从白刷到新图片
#define FRAME_END_LEN		26
unsigned char wave_end[4][FRAME_END_LEN]=
{
0,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,0,				//GC3->GC0
0,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,1,1,1,0,0,0,0,0,0,				//GC3->GC1
0,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,1,0,0,0,0,0,0,0,0,				//GC3->GC2
0,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,				//GC3->GC3
};

#if 1
//黑白黑白刷屏。最终到达白背景
#define FRAME_INIT_LEN 		2
unsigned long wave_init[FRAME_INIT_LEN]=
{
//0x55,0x55,0x55,0x55,
//0xaa,0xaa,0xaa,0xaa,
//0x55,0x55,0x55,0x55,
//0,
//0xaa,0xaa,0xaa,0xaa,
//0x55,0x55,0x55,0x55,
//0xaa,0xaa,0xaa,0xaa,
0,0,
};

#endif

*/



//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//全局变量
//生成于Ram的波形数据表
unsigned char g_dest_data[256];				//送到电子纸的一行数据缓存

unsigned int loop = 0;						//循环计数
unsigned int column = 0;					//电子纸行驱动用 变量
unsigned int old_data;						//电子纸行驱动用 变量
unsigned int new_data;						//电子纸行驱动用 变量

unsigned long item_dis;		

unsigned char FRAME_BEGIN_LEN;
unsigned char FRAME_END_LEN;
unsigned char FRAME_INIT_LEN;

//unsigned char wave_begin_table[256][FRAME_BEGIN_LEN]={0};
//unsigned char wave_end_table[256][FRAME_END_LEN]={0};

unsigned char wave_begin_table[256][30]={0};
unsigned char wave_end_table[256][30]={0};

unsigned char wave_begin[4][30]={0};
unsigned char wave_end[4][30]={0};
unsigned long wave_init[30]={0};

unsigned int epaper_line,epaper_row;

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#if 1
void line_data_init(u8 frame)
{
	int i,n;
	
	n = epaper_line/4;

	for(i=0; i<n; i++)
	{
		g_dest_data[i] = wave_init[frame];	
	}	
}
#endif

#if 1
void line_begin_pic(u8 *old_pic, u8 frame)
{
	int i,n;
	
	n = epaper_line/4;
	
	for(i=0; i<n; i++)
	{
		g_dest_data[i] = wave_begin_table[old_pic[i]][frame];	
	}	
}
#endif

void line_end_pic(u8 *new_pic, u8 frame)
{
	int i,n;
	
	n = epaper_line/4;
		
	for(i=0; i<n; i++)
	{
		g_dest_data[i] = wave_end_table[new_pic[i]][frame];	
	}	
}


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//图片驱动显示
unsigned char u_test=1;
void EPD_Display_PIC(void)
{
  int line, frame,n,k;
	unsigned char *ptr;
	
	n = epaper_line/4;

	EPD_PowerOn();
//	Delay(0xfffff);
	Delay(0xfff);
	
#if 1
	//先读旧图片，刷到白
	
	if(u_test == 0)	
	{
		if(epaper_flag == ESL43)
			ptr = (unsigned char *)(gImage_lanse_43);			//acgs_43   gImage_lanse_43
		else if(epaper_flag == ESL35)
			ptr = (unsigned char *)(gImage_shangxia);
		else if(epaper_flag == ESL60)
			ptr = (unsigned char *)(gImage_lanse_43);	
		else if(epaper_flag == ESL80)
			ptr = (unsigned char *)(ac_80);
	}
	else
	{
		if(epaper_flag == ESL43)
			ptr = (unsigned char *)(gImage_yinxin_43);
		else if(epaper_flag == ESL35)
			ptr = (unsigned char *)(gImage_jiyu);		
		else if(epaper_flag == ESL60)
			ptr = (unsigned char *)(gImage_yinxin_43);
		else if(epaper_flag == ESL80)
			ptr = (unsigned char *)(gImage_test_80);
	}
	
	for(frame=0; frame<FRAME_BEGIN_LEN; frame++)			
	{
    	
		EPD_Start_Scan();
		Delay(0xff);
		for(line=0; line<epaper_row; line++)
		{
			line_begin_pic(ptr + line*n, frame);			//42ms
			EPD_Send_Row_Data( g_dest_data );				//40ms	
		}
		Delay(0xff);
		EPD_Send_Row_Data( g_dest_data );					//最后一行还需GATE CLK,故再传一行没用数据
	}
#endif

	
#if 0
	//黑白黑白刷屏。最终到达白背景
	for(frame=0; frame<FRAME_INIT_LEN; frame++)			
	{
		
		EPD_Start_Scan();
		for(line=0; line<epaper_row; line++)
		{
			line_data_init(frame);							//14ms
			EPD_Send_Row_Data( g_dest_data );				//40ms
			
		}
		EPD_Send_Row_Data( g_dest_data );					//最后一行还需GATE CLK,故再传一行没用数据

	}
#endif
//	Delay(0xfffff);
	Delay(0xfff);
#if 1	
	//从白刷到新图片

	if(u_test != 0)	
	{
		if(epaper_flag == ESL43)
			ptr = (unsigned char *)(gImage_lanse_43);	//acgs_43    gImage_lanse_43
		else if(epaper_flag == ESL35)
			ptr = (unsigned char *)(gImage_shangxia);
		else if(epaper_flag == ESL60)
			ptr = (unsigned char *)(gImage_lanse_43);			
		else if(epaper_flag == ESL80)
			ptr = (unsigned char *)(ac_80);
	}
	else
	{
		if(epaper_flag == ESL43)
			ptr = (unsigned char *)(gImage_yinxin_43);
		else if(epaper_flag == ESL35)
			ptr = (unsigned char *)(gImage_jiyu);		
		else if(epaper_flag == ESL60)
			ptr = (unsigned char *)(gImage_yinxin_43);
		else if(epaper_flag == ESL80)
			ptr = (unsigned char *)(gImage_test_80);
	}	
	
	if((epaper_flag == ESL43) || (epaper_flag == ESL35))
		k = FRAME_END_LEN-2;
	else if((epaper_flag == ESL60) || (epaper_flag == ESL80))
		k = FRAME_END_LEN;

	for(frame=0; frame<k; frame++)			//FRAME_END_LEN-2
	{
		EPD_Start_Scan();
		for(line=0; line<epaper_row; line++)
		{
			line_end_pic(ptr + line*n, frame);			//42ms
			EPD_Send_Row_Data( g_dest_data );				//40ms

		}
		EPD_Send_Row_Data( g_dest_data );					//最后一行还需GATE CLK,故再传一行没用数据
	}
#endif
//	Delay(0xfffff);
	Delay(0xfff);
	EPD_PowerOff();
}



//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//延时函数
void Delay(vu32 nCount)
{

  for(; nCount != 0; nCount--);

}

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//生成于RAM中的波形表，以提高屏的扫描速度

void make_wave_table(void)
{
	int frame, num;
	unsigned char tmp,value;

	if((epaper_flag == ESL43) || (epaper_flag == ESL35))
	{
		//wave_begin_table
		for(frame=0; frame<FRAME_BEGIN_LEN; frame++)
		{		
			for(num=0; num<256; num++)
			{
				tmp = 0;
				tmp = wave_begin[(num>>6)&0x3][frame];
						
				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_begin[(num>>4)&0x3][frame];

				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_begin[(num>>2)&0x3][frame];

				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_begin[(num)&0x3][frame];

				value = 0;
				value = (tmp <<6) & 0xc0;
				value += (tmp<<2) & 0x30;
				value += (tmp>>2) & 0x0c;
				value += (tmp>>6) & 0x03;
				wave_begin_table[num][frame] = value;

			}
		}

		//wave_end_table
		for(frame=0; frame<FRAME_END_LEN; frame++)
		{		
			for(num=0; num<256; num++)
			{
				tmp = 0;
				tmp = wave_end[(num>>6)&0x3][frame];
						
				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_end[(num>>4)&0x3][frame];

				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_end[(num>>2)&0x3][frame];

				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_end[(num)&0x3][frame];

				value = 0;
				value = (tmp <<6) & 0xc0;
				value += (tmp<<2) & 0x30;
				value += (tmp>>2) & 0x0c;
				value += (tmp>>6) & 0x03;
				wave_end_table[num][frame] = value;		
			}
		}
	}
	else if((epaper_flag == ESL60) || (epaper_flag == ESL80))
	{
		for(frame=0; frame<FRAME_BEGIN_LEN; frame++)
		{		
			for(num=0; num<256; num++)
			{
				tmp = 0;
				tmp = wave_begin[(num)&0x3][frame];
						
				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_begin[(num>>2)&0x3][frame];

				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_begin[(num>>4)&0x3][frame];

				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_begin[(num>>6)&0x3][frame];

				value = 0;
				value = (tmp <<6) & 0xc0;
				value += (tmp<<2) & 0x30;
				value += (tmp>>2) & 0x0c;
				value += (tmp>>6) & 0x03;
				wave_begin_table[num][frame] = value;
			}
		}

		//wave_end_table
		for(frame=0; frame<FRAME_END_LEN; frame++)
		{		
			for(num=0; num<256; num++)
			{
				tmp = 0;
				tmp = wave_end[(num)&0x3][frame];
						
				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_end[(num>>2)&0x3][frame];

				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_end[(num>>4)&0x3][frame];

				tmp = tmp<< 2;
				tmp &= 0xfffc;
				tmp |= wave_end[(num>>6)&0x3][frame];

				value = 0;
				value = (tmp <<6) & 0xc0;
				value += (tmp<<2) & 0x30;
				value += (tmp>>2) & 0x0c;
				value += (tmp>>6) & 0x03;
				wave_end_table[num][frame] = value;
			}
		}		
	}
		
}

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//高压和VCOM开
void EPD_PowerOn(void)
{
	GPIO_SetBits(VNEGGVEE_CTR_PORT, VNEGGVEE_CTR);  //pa1
	Delay(0xf);
	GPIO_SetBits(VPOS15_CTR_PORT, VPOS15_CTR);      //pa2
	Delay(0xf);
	GPIO_SetBits(GVDD22_CTR_PORT, GVDD22_CTR);      //pa3
	Delay(0xf);
	
	GPIO_SetBits(VCOM_CTR_PORT, VCOM_CTR);					//VCOM = -V
	Delay(0xff);
	
}

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//高压和VCOM关  
void EPD_PowerOff(void)
{
	GPIO_ResetBits(VCOM_CTR_PORT, VCOM_CTR);				//VCOM = 0
	Delay(0x2ff);
	
	GPIO_ResetBits(GVDD22_CTR_PORT, GVDD22_CTR);
	Delay(0x2f);
	GPIO_ResetBits(VPOS15_CTR_PORT, VPOS15_CTR);
	Delay(0x2f);
	GPIO_ResetBits(VNEGGVEE_CTR_PORT, VNEGGVEE_CTR);
	Delay(0x2ff);
	
}


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//电子纸IO口初始化

void EPD_Init(void)
{
//	GPIO_InitTypeDef  GPIO_InitStructure;
//	GPIO_InitStructure.GPIO_Pin = PORT_C_PIN_OUT; 
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
//  GPIO_InitStructure.GPIO_Speed = GPIO_SPEED_EPD;//100MHz
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;				//GPIO_PuPd_UP;GPIO_PuPd_NOPULL//上拉
//	GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_ResetBits(EPD_SHR_PORT, EPD_SHR);               	//Shift direction source driver	  
	GPIO_SetBits(EPD_GMODE1_PORT, EPD_GMODE1);				//one pulse mode	
	GPIO_SetBits(EPD_GMODE2_PORT, EPD_GMODE2);				//one pulse mode
	GPIO_SetBits(EPD_XRL_PORT, EPD_XRL);               		//Shift direction gate driver
	

	EPD_PowerOff();
	
	EPD_LE_L;
	EPD_CL_L;
	EPD_OE_L;
	EPD_SPH_H;
	EPD_XSPV_H;
	EPD_CLK_L;
}
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//EPD开始时序
void EPD_Start_Scan(void)
{
	   
	EPD_XSPV_H;                                             //XSPV先高

	loop = 2;                                       		//DCLK跑2个时钟
	while(loop--)                            
	{
		EPD_CLK_L;
		Delay(0xf);	
		EPD_CLK_H;
		Delay(0xf);

	}
 
	EPD_XSPV_L;                                             //XSPV再低

	loop = 2;
	while(loop--)
	{
		EPD_CLK_L;                    						//DCLK跑2个时钟
		Delay(0xf);
		EPD_CLK_H;
		Delay(0xf);
	}

	EPD_XSPV_H;                                             //XSPV  变高

	loop = 2;
	while(loop--)
	{
		EPD_CLK_L;
		Delay(0xf);
		EPD_CLK_H;
		Delay(0xf);
	}
}


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//行扫描
#define DISP_DELAY 4
void EPD_Send_Row_Data(unsigned char *pArray )      					//  送数据，
{
	unsigned int n,delay_temp;
	
	if(epaper_flag == ESL43)
		delay_temp = 2*DISP_DELAY;
	else if(epaper_flag == ESL35)
		delay_temp = 3*DISP_DELAY;
	else if(epaper_flag == ESL60)
		delay_temp = 3*DISP_DELAY;
	else if(epaper_flag == ESL80)
		delay_temp = 3*DISP_DELAY;
	
	n = epaper_line/4;
	
	EPD_LE_H;                                              	//LE 先低
	EPD_CL_L;
	EPD_CL_H;
	EPD_CL_L;
	EPD_CL_H;
	
	
	EPD_LE_L;  												//LE 高
	EPD_CL_L;
	EPD_CL_H;
	EPD_CL_L;
	EPD_CL_H;
	
	EPD_OE_H;                                              	//OE 先高
	EPD_CL_L;
	EPD_CL_H;
	EPD_CL_L;
	EPD_CL_H;
	EPD_SPH_L;                                              //STL 先低，

	old_data = (uint16_t)(EPD_DB_PORT->ODR);
	old_data &= 0xFF00;
	for(column= 0; column < n; column++)          		// 写一行数据， 
	{
		new_data = (old_data | ((u16)pArray[column]));
  	(EPD_DB_PORT->ODR) = new_data;

		EPD_CL_L;
		Delay(delay_temp);
	
		EPD_CL_H;
		Delay(delay_temp);

	}

	EPD_SPH_H;                                             	//STL 高
	EPD_CL_L;
	EPD_CL_H;
	EPD_CL_L;
	EPD_CL_H;


  EPD_CLK_L;                                               //DCLK  变低
	EPD_OE_L;                                                //OE  变低
	EPD_CL_L;
	EPD_CL_H;
	EPD_CL_L;
	EPD_CL_H;
	Delay(delay_temp);

	EPD_CLK_H;                                               //DCLK  变高( DCLK 一个时钟)
	Delay(delay_temp);

}


void Epaper_Init(void)
{
	
	GPIO_InitTypeDef  GPIO_InitStructure;
		//使能系统使用到的GPIO的时钟
	RCC_AHB1PeriphClockCmd(RCC_GPIO_USE, ENABLE); 

		//配置EPD使用到的GPIO的参数
	GPIO_InitStructure.GPIO_Pin = PORT_B_PIN_OUT; 				//
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_SPEED_EPD;//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;				//GPIO_PuPd_UP;//上拉
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = PORT_C_PIN_OUT; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_SPEED_EPD;//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;				//GPIO_PuPd_UP;//上拉�
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = PORT_E_PIN_OUT; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_SPEED_EPD;//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;				//GPIO_PuPd_UP;//上拉
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = PORT_F_PIN_OUT; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_SPEED_EPD;//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;	//GPIO_PuPd_UP;//上拉
	GPIO_Init(GPIOF, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = PORT_G_PIN_OUT; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_SPEED_EPD;//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;				//GPIO_PuPd_UP;//上拉
	GPIO_Init(GPIOG, &GPIO_InitStructure);
	
//	GPIO_InitStructure.GPIO_Pin = PORT_C_PIN_IN; //
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
//  GPIO_InitStructure.GPIO_Speed = GPIO_SPEED_EPD;//100M
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
//  GPIO_Init(GPIOC, &GPIO_InitStructure);//初始化GPIOE2,3,4
	
}
