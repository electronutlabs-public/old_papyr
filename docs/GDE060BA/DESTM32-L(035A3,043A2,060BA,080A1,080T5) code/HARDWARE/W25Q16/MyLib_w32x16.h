#ifdef   MyLib_W32X16_GLOBALS	     //防止重复定义
  #define  MyLib_W32X16_EXT
#else
  #define  MyLib_W32X16_EXT extern
#endif

#ifndef MyLib_W32X16_H
	#define MyLib_W32X16_H			//防止重复调用

#include "stm32f4xx.h"	
	
#define Font_Item_Adrr			0x0000				//字库索引起始地址			每条保存一个字库的128字节（保存一个字库）
#define Flash_Check_Adrr		0x0100				//字库索引起始地址			每条保存一个字库的128字节（保存一个字库）
#define Picture_Item_Adrr		0x1000				//图片索引起始地址			按照实际的图片大小，一条表示一个图片
#define Font_Adrr						0x2000				//字库数据起始地址			32*32  GBK字库  3184025字节数据即0x310000个字节，如果是64*64点阵则为0xC40000
#define Picture_Adrr				0xD00000			//图片数据起始地址

	
extern unsigned long Flash_Erase_Delay;
extern unsigned char Flash_Erase_Flag;

/*********************************************************************
* 函数:  unsigned long MyLib_W32X_Flash_Id_Read (SPI_TypeDef * SPI)
* 功能:  读芯片的ID值
* 输入:  无
* 输出:  芯片ID
* 注意:  可以用这个函数判断写篇的类型，如果返回数据不对，表示
*        硬件部分有错误	
********************************************************************/		
	MyLib_W32X16_EXT unsigned long MyLib_W32X_Flash_Id_Read (SPI_TypeDef * SPI);
/*********************************************************************
* 函数:  void MyLib_W32X_ChipErase(SPI_TypeDef * SPI)
* 功能:  整片擦除
* 输入:  无
* 输出:  无
* 注意:  通过读出操作验证是否擦出成功	
********************************************************************/
	MyLib_W32X16_EXT void MyLib_W32X_ChipErase(SPI_TypeDef * SPI);
/*********************************************************************
* 函数:  void MyLib_W32X_SectorErase(SPI_TypeDef * SPI,unsigned long Addre24)
* 功能:  扇区擦除
* 输入:  Addre24  扇区的首地址
* 输出:  无
* 注意:  通过读出操作验证是否擦出成功	
********************************************************************/
	MyLib_W32X16_EXT void MyLib_W32X_SectorErase(SPI_TypeDef * SPI,unsigned long Addre24);
/*********************************************************************
* 函数:  void MyLib_W32X_Flash_Byte_Read (SPI_TypeDef * SPI,unsigned long Addre24,unsigned char *Dest,unsigned long Quantity)
* 功能:  读操作
* 输入:  Addre24  	读开始的地址
*        *Dest 		读出数据的保存地址
*        Quantity 	读出长度
* 输出:  无
* 注意:  
********************************************************************/
	MyLib_W32X16_EXT void MyLib_W32X_Flash_Byte_Read (SPI_TypeDef * SPI,unsigned long Addre24,unsigned char *Dest,unsigned long Quantity);  //从Flash里读出小于256字节数
/*********************************************************************
* 函数:  void MyLib_W32X_Flash_Byte_Write(SPI_TypeDef * SPI,unsigned long Addre24,unsigned char *Src,unsigned long Quantity)
* 功能:  写操作
* 输入:  Addre24  	写开始的地址
*        *Src 		写入数据的首地址
*        Quantity 	写入长度
* 输出:  无
* 注意: Addre24+Quantity不能超过当前扇区的长度。 
********************************************************************/
	MyLib_W32X16_EXT void MyLib_W32X_Flash_Byte_Write(SPI_TypeDef * SPI,unsigned long Addre24,unsigned char *Src,unsigned long Quantity);
/*********************************************************************
* 函数:  void MyLib_W32X_Flash_Byte_Write(SPI_TypeDef * SPI,unsigned long Addre24,unsigned char *Src,unsigned long Quantity)
* 功能:  写操作
* 输入:  Addre24  	写开始的地址
*        *Src 		写入数据的首地址
*        Quantity 	写入长度
* 输出:  无
* 注意: Addre24+Quantity不能超过当前扇区的长度。 
********************************************************************/
	MyLib_W32X16_EXT void MyLib_W32X_Flash_Byte_Read_Dma (SPI_TypeDef * SPI,unsigned long Addre24,unsigned char *Dest,unsigned long Quantity);
//	MyLib_W32X16_EXT void SPI_DMAConfig(SPI_TypeDef * SPI,unsigned char *Dest,unsigned long Quantity);

	MyLib_W32X16_EXT bool MyLib_W32X_Flash_Byte_Write_check(SPI_TypeDef * SPI,unsigned long Addre24,unsigned char *Src,unsigned long Quantity);

	MyLib_W32X16_EXT void Delay_SPI(unsigned long i);
	
	MyLib_W32X16_EXT void Flash_Init_Check(void);
#endif
