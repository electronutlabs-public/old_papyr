
#define MyLib_W32X16AUX_GLOBALS
#include "stm32f4xx.h"
#include "stm32f4xx_spi.h"
#include "MyLib_w32x16.h"
#include "MyLib_w32x16aux.h"



//SPI1速度设置函数
//SPI速度=fAPB2/分频系数
//@ref SPI_BaudRate_Prescaler:SPI_BaudRatePrescaler_2~SPI_BaudRatePrescaler_256  
//fAPB2时钟一般为84Mhz：
void SPI1_SetSpeed(u8 SPI_BaudRatePrescaler)
{
  assert_param(IS_SPI_BAUDRATE_PRESCALER(SPI_BaudRatePrescaler));//判断有效性
	SPI1->CR1&=0XFFC7;//位3-5清零，用来设置波特率
	SPI1->CR1|=SPI_BaudRatePrescaler;	//设置SPI1速度 
	SPI_Cmd(SPI1,ENABLE); //使能SPI1
} 
//SPI1 读写一个字节
//TxData:要写入的字节
//返回值:读取到的字节
u8 SPI1_ReadWriteByte(u8 TxData)
{		 			 
 
  while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET){}//等待发送区空  
	
	SPI_I2S_SendData(SPI1, TxData); //通过外设SPIx发送一个byte  数据
		
  while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET){} //等待接收完一个byte  
 
	return SPI_I2S_ReceiveData(SPI1); //返回通过SPIx最近接收的数据	
 		    
}


void MyLib_SPI_Config(SPI_TypeDef * SPI,uint16_t reMap,\
		uint16_t SPI_Direction,\
		uint16_t SPI_Mode,\
		uint16_t SPI_DataSize ,\
		uint16_t SPI_CPOL,\
		uint16_t SPI_CPHA	,\
		uint16_t SPI_NSS  ,\
		uint16_t SPI_BaudRatePrescaler,\
		uint16_t SPI_FirstBit ,\
		uint16_t SPI_CRCPolynomial )
{
    
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE); 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1,ENABLE);			//使能SPI1时钟
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7; 				//	PA5--CLK   PA6---RX(MISO)   PA7---TX(MOSI)	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;			//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;		//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_SPI;		//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4; 				//	PA4    CS	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;		//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_SPI;		//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(GPIOA, &GPIO_InitStructure);	

	
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource5,GPIO_AF_SPI1); //PA5复用为 SPI1
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource6,GPIO_AF_SPI1); //PA6复用为 SPI1
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource7,GPIO_AF_SPI1); //PA7复用为 SPI1
	
	//这里只针对SPI口初始化
	RCC_APB2PeriphResetCmd(RCC_APB2Periph_SPI1,ENABLE);//复位SPI1
	RCC_APB2PeriphResetCmd(RCC_APB2Periph_SPI1,DISABLE);//停止复位SPI1
	

	SPI_InitStructure.SPI_Direction = SPI_Direction;
	SPI_InitStructure.SPI_Mode = SPI_Mode;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA;
	SPI_InitStructure.SPI_NSS = SPI_NSS;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit;
	SPI_InitStructure.SPI_CRCPolynomial = SPI_CRCPolynomial;
	SPI_Init(SPI, &SPI_InitStructure);
	
	/* Enable SPI  */
	SPI_Cmd(SPI, ENABLE);
	SPI1_SetSpeed(SPI_BaudRatePrescaler_2);		//设置为42M时钟,高速模式
	
	SPI1_ReadWriteByte(0xff);//启动传输	

}




void MyLib_CS_Flash_L()
{
	GPIO_ResetBits(GPIOA, GPIO_Pin_4);
}
void MyLib_CS_Flash_H()
{
	GPIO_SetBits(GPIOA, GPIO_Pin_4);
}

/*********************************************************************************************************
** Function name:           Send_Byte
**
** Descriptions:            通过硬件SPI发送一个字节到SST25VF016B
**
** input parameters:        data   发送的数据
** output parameters:       NONE
**                          
** Returned value:          NONE
**

**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
void MyLib_Send_Byte(SPI_TypeDef * SPI,unsigned char data)
{
	/* Loop while DR register in not emplty */
	//while(SPI_I2S_GetFlagStatus(MyLib_SPI, SPI_I2S_FLAG_TXE) == RESET);
	while((SPI->SR & 0x0002) == (uint16_t)0);
	
	/* Send byte through the SPI1 peripheral */
	//SPI_I2S_SendData(MyLib_SPI, data);
	SPI->DR = data;
	/* Wait to receive a byte */
	//while(SPI_I2S_GetFlagStatus(MyLib_SPI, SPI_I2S_FLAG_RXNE) == RESET);
	while((SPI->SR & 0x0001) == (uint16_t)0);
	//SPI_I2S_ReceiveData(MyLib_SPI);
	SPI->DR;
                                          
}
/*********************************************************************************************************
** Function name:           Get_Byte
**
** Descriptions:            通过硬件SPI接口接收一个字节到处理器	
**
** input parameters:        NONE
** output parameters:       NONE
**                          
** Returned value:          ReadData  读回的数据
**

**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/

unsigned char MyLib_Get_Byte(SPI_TypeDef * SPI)
{

	while((SPI->SR & 0x0002) == (uint16_t)0);
	/* Send byte through the SPI1 peripheral */
	//SPI_I2S_SendData(MyLib_SPI, data);
	SPI->DR = 0xff;
	/* Wait to receive a byte */
	//while(SPI_I2S_GetFlagStatus(MyLib_SPI, SPI_I2S_FLAG_RXNE) == RESET);
	while((SPI->SR & 0x0001) == (uint16_t)0);
	//SPI_I2S_ReceiveData(MyLib_SPI);
	return SPI->DR;

}

void SPI_Config(void)
{
		MyLib_SPI_Config(W32X16,0,\
		SPI_Direction_2Lines_FullDuplex,\
		SPI_Mode_Master,\
		SPI_DataSize_8b,\
		SPI_CPOL_Low,\
		SPI_CPHA_1Edge,\
		SPI_NSS_Soft,\
		SPI_BaudRatePrescaler_4,\
		SPI_FirstBit_MSB,\
		7);		       //SPI初始化

}

