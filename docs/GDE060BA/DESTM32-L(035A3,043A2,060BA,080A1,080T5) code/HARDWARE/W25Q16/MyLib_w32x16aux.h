//避免变量重复定义
#ifdef   MyLib_W32X16AUX_GLOBALS
	#define  MyLib_W32X16AUX_EXT
#else
	#define  MyLib_W32X16AUX_EXT extern
#endif
//避免文件被重复包含
#ifndef   MyLib_W32X16AUX_H
    #define  MyLib_W32X16AUX_H

#include "stm32f4xx.h"    

#define  W32X16 SPI1
#define GPIO_Speed_SPI 	GPIO_Speed_50MHz
	
	MyLib_W32X16AUX_EXT void MyLib_SPI_Config(SPI_TypeDef * SPI,uint16_t reMap,\
		uint16_t SPI_Direction,\
		uint16_t SPI_Mode,\
		uint16_t SPI_DataSize ,\
		uint16_t SPI_CPOL,\
		uint16_t SPI_CPHA	,\
		uint16_t SPI_NSS  ,\
		uint16_t SPI_BaudRatePrescaler,\
		uint16_t SPI_FirstBit ,\
		uint16_t SPI_CRCPolynomial );
	MyLib_W32X16AUX_EXT void MyLib_CS_Flash_L(void);
	MyLib_W32X16AUX_EXT void MyLib_CS_Flash_H(void);
	MyLib_W32X16AUX_EXT void MyLib_Send_Byte(SPI_TypeDef * SPI,unsigned char data);
	MyLib_W32X16AUX_EXT unsigned char MyLib_Get_Byte(SPI_TypeDef * SPI);

	MyLib_W32X16AUX_EXT void SPI_Config(void);
#endif
