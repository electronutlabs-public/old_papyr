#ifndef __USART3_H
#define __USART3_H	

#include "sys.h" 



extern unsigned char USART3_Buffer_Rx[256];
extern unsigned char USART3DMA_Length;
extern unsigned char USART3_templenth,USART3_Start;
extern unsigned char USART3_ScanDelay,USART3_ScanDelay_Flag;

extern unsigned char USART1_Buffer_Rx[256];
extern unsigned char USART1DMA_Length;
extern unsigned char USART1_templenth,USART1_Start;
extern unsigned char USART1_ScanDelay,USART1_ScanDelay_Flag;


extern unsigned char UART4_Buffer_Rx[256];
extern unsigned char UART4_Tx[20];
extern unsigned char UART4DMA_Length;
extern unsigned char UART4_templenth,UART4_Start;
extern unsigned char UART4_ScanDelay,UART4_ScanDelay_Flag;


void USART3_Oper(void);
void USART1_Oper(void);
void UART4_Oper(void);

#endif
