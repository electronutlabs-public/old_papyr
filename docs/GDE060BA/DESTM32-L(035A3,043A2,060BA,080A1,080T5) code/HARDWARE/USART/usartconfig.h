#ifndef __USARTCONFIG_H
#define __USARTCONFIG_H	


#include "sys.h" 

#define GPIO_Speed_USART2 	GPIO_Speed_100MHz
#define GPIO_Speed_USART3 	GPIO_Speed_100MHz


void MYUSART_Init(void);
void MYDMA_Enable(DMA_Stream_TypeDef *DMA_Streamx,u16 BufferSize);
void MyLib_UsartDMATx(USART_TypeDef* USARTx,DMA_Stream_TypeDef *DMA_Streamx,u32 Channel,u32 Memory0BaseAddr,u16 BufferSize,u32 DMA_Mode);
void MyLib_UsartDMARx(USART_TypeDef* USARTx,DMA_Stream_TypeDef *DMA_Streamx,u32 Channel,u32 Memory0BaseAddr,u16 BufferSize,u32 DMA_Mode);







#endif
