#include "usart2.h"
#include "MyLib_w32x16.h"
#include "MyLib_w32x16aux.h"
#include "usartconfig.h"
#include "mathchange.h"	



unsigned char USART2_Buffer_Rx[256];
unsigned char USART2_Tx[20];
unsigned char USART2DMA_Length=0;
unsigned char USART2_templenth,USART2_Start;
unsigned char USART2_ScanDelay,USART2_ScanDelay_Flag;
unsigned long Item_Font=0;
unsigned char pic_rx_flag=0;			//显示内容接收进行中的标志
unsigned char pic_rx_item=0;			//显示内容接收进行中的第几条数据
unsigned char font_erase_flag = 0;
unsigned char disp_update_flag=0;



/****************************USART通信处理*******************************/
/*************************************************************************
数据格式：

0xFE LEN0...LEN3 Adrr1.....Adrr6 Cmd Data1.....Datan Check 0x16

0xFE	起始符

LEN		数据长度4个字节 (包括Adrr  Cmd  Data)

Adrr	7字节地址

Cmd		控制命令

Data	数据域

Check 校验和

0x16	结束符

**************************************************************************/

/**************************************************************************
** Function name:           read_oper
**
** Descriptions:            串口2的通信处理
**
** input parameters:          
** output parameters:       
**                          
** Returned value:          
**************************************************************************/
unsigned long save_disitem[2];
void write_oper(unsigned char *p_tx)
{
	unsigned int m,n;

	
	if(Data_D0 == 0x00)
	{
		if(Data_D1 == 0x01)												//字库更新命令
		{
			if(font_erase_flag == 0x55)							//更新字库前需先擦除
			{
				MyLib_W32X_Flash_Byte_Write(W32X16,Font_Adrr+Item_Font*128,&USART2_Buffer_Rx[DATA_START],128);						//每次最多接收汉字内码128字节
				Item_Font++;	
				
				*(p_tx+12) |= 0x80;										//正确接收返回cmd|0x80
			}
			else
				*(p_tx+12) |= 0xC0;										//异常返回cmd|0xC0
		}
		else if(Data_D1 == 0x02)									//显示更新命令
		{
			if(pic_rx_flag == 0)
			{
				pic_rx_item = 0;
				MyLib_W32X_Flash_Byte_Read(W32X16,Picture_Item_Adrr,(unsigned char *)&save_disitem[0],8);	  //保存索引
				save_disitem[0] = save_disitem[0]+1;
				
				if(save_disitem[0] <= picture_item_max)
				{	//一次最多保存256字节的图片信息 64*64点阵 8寸电子纸1024*768  再加上x和y方向的放大倍数，x和y方向的显示的起始坐标，四个数据均为int型，两个字节，共8字节。
					MyLib_W32X_Flash_Byte_Write(W32X16,Picture_Adrr+save_disitem[0]*picture_len_max+pic_rx_item*128,(unsigned char *)&USART2_Buffer_Rx[DATA_START],128);
					
					MyLib_W32X_SectorErase(W32X16,Picture_Item_Adrr);
					
					MyLib_W32X_Flash_Byte_Write_check(W32X16,Picture_Item_Adrr,(unsigned char *)&save_disitem[0],8);	

					pic_rx_flag = 0x55;
					pic_rx_item++;
					*(p_tx+12) |= 0x80;										//正确接收返回cmd|0x80
				}
				else
					*(p_tx+12) |= 0xC0;										//如果大于最大的保存条数，则不再保存
			}
			else
			{
				MyLib_W32X_Flash_Byte_Write(W32X16,Picture_Adrr+save_disitem[0]*picture_len_max+pic_rx_item*128,(unsigned char *)&USART2_Buffer_Rx[DATA_START],128);
				pic_rx_item++;	
				*(p_tx+12) |= 0x80;										//正确接收返回cmd|0x80
			}
			
		}
		else if(Data_D1 == 0x03)						//flash片擦除命令，擦除后需要等到4-5s才能进行字库或者显示更新命令的操作，因为flash擦除需要时间
		{
			pic_rx_item = 0;
			pic_rx_flag = 0;
			MyLib_W32X_ChipErase(W32X16);	
			*(p_tx+12) |= 0x80;										//正确接收返回cmd|0x80		
		}
		else if(Data_D1 == 0x04)						//块擦除命令，更新字体前需要先擦除字库部分的flash
		{
			m = (Picture_Adrr-Font_Adrr)/0x1000;
			for(n=0;n<m;n++)
				MyLib_W32X_SectorErase(W32X16,Font_Adrr+n*0x1000);
			font_erase_flag = 0x55;
			
			*(p_tx+12) |= 0x80;										//正确接收返回cmd|0x80
		}
		else if(Data_D1 == 0x05)								//对显示内容的擦写
		{
			m = (0x1000000-Picture_Adrr)/0x1000;
			for(n=0;n<m;n++)
				MyLib_W32X_SectorErase(W32X16,Picture_Adrr+n*0x1000);
			
			MyLib_W32X_SectorErase(W32X16,Picture_Item_Adrr);
			
			*(p_tx+12) |= 0x80;										//正确接收返回cmd|0x80		
		}
	}
}

void font_end_oper(void)
{
	if(Item_Font != 0)
	{
		Item_Font = 0;
		font_erase_flag = 0;
	}
}

void pic_end_oper(void)
{
	if(pic_rx_flag != 0)
	{
		pic_rx_item = 0;
		pic_rx_flag = 0;		
		disp_update_flag = 0x55;
	}

}
void communication_oper(void)
{
//	unsigned char usart2_tx[20];
	unsigned char tx_len;
	unsigned char i;
	
	for(i=0;i<15;i++)
		USART2_Tx[i] = USART2_Buffer_Rx[i];		//将接收数据0xFE LEN Adrr Cmd赋值给发送buff

	if(USART_DataCheckWifi(USART2_Buffer_Rx))
	{	
		switch(Data_Cmd)
		{
			case 0x02:
				write_oper(USART2_Tx);
				break;
			
			case 0xE1:
				font_end_oper();
				break;
			
			case 0xE2:
				pic_end_oper();
				break;
			
			default:
				break;
		}
	}
	else
	{
		USART2_Tx[12] &= 0x0f;
		USART2_Tx[12] |= 0xc0;
	}
		
	if((Data_Cmd != 0xE1) && (Data_Cmd != 0xE2))
	{
		tx_len = 10; 
		LONG_CHAR(tx_len,&USART2_Tx[1]);
		USART2_Tx[tx_len+5] = Sum_Check(&USART2_Tx[1],tx_len+4);
		USART2_Tx[tx_len+6] = 0x16;
		
		tx_len = tx_len+7;
	}
		
	MyLib_UsartDMARx(USART2,DMA1_Stream5,DMA_Channel_4,(u32)&USART2_Buffer_Rx[0],256,DMA_Mode_Normal);
	
	if((Data_Cmd != 0xE1) && (Data_Cmd != 0xE2))	
		MyLib_UsartDMATx(USART2,DMA1_Stream6,DMA_Channel_4,(u32)&USART2_Tx[0],tx_len,DMA_Mode_Normal);	
	
}
/*********************************************************************************************************
** Function name:           USART2_Oper
**
** Descriptions:            串口2通信处理
**
** input parameters:        
** output parameters:       
**                          
** Returned value:          

*********************************************************************************************************/

void USART2_Oper(void)
{
	unsigned char kk_test[6],i;
	
	if(USART2_Start == 0x55)
	{
		if(USART2_ScanDelay_Flag == 0x55)
		{
			USART2_templenth =  USART2DMA_Length;
			USART2DMA_Length = DMA_GetCurrDataCounter(DMA1_Stream5);//获取剩余长度
			USART2DMA_Length = 256-USART2DMA_Length; 

			if(USART2DMA_Length != USART2_templenth) 			//延时5ms，检测是否有新的数据，如果有，重新开始延时
			{
				USART2_ScanDelay = 0x05;
				USART2_ScanDelay_Flag = 0;
			}
			else										//延时后如果没有新数据，初始化DMA，清延时计数器，清延时时间到的flag，清接收开始flag
			{	
				DMA_Cmd(DMA1_Stream5, DISABLE);
				
				USART2_ScanDelay = 0x00;
				USART2_ScanDelay_Flag = 0;
				USART2_Start = 0;
				
//				communication_oper();
				
				DMA_Cmd(DMA1_Stream5, DISABLE);
				
				kk_test[0] = 0X01;		//Adress
				kk_test[1] = 0X01;		//cmd
				kk_test[2] = USART2_Buffer_Rx[2];		//帧系序列   需跟收到的数据一致
				kk_test[3] = 0X00;		//数据长度0
				kk_test[4] = 0X00;		//数据域
				kk_test[5] = 0X00;
				
				for(i=0;i<5;i++)
					kk_test[5] += kk_test[i];		
				kk_test[5] = 0xff-kk_test[5]+1;
					
				MyLib_W32X_Flash_Byte_Write(W32X16,Font_Adrr+Item_Font*128,&USART2_Buffer_Rx[4],USART2_Buffer_Rx[3]);
				Item_Font++;

			
				MyLib_UsartDMARx(USART2,DMA1_Stream5,DMA_Channel_4,(u32)USART2_Buffer_Rx,256,DMA_Mode_Normal);
				
				MyLib_UsartDMATx(USART2,DMA1_Stream6,DMA_Channel_4,(u32)kk_test,6,DMA_Mode_Normal);	
				
				USART2_ScanDelay = 0x00;
				USART2_ScanDelay_Flag = 0;
				USART2_Start = 0;
			}
		}
	}
	else															//如果接收没有开始 
	{
		USART2_templenth =  USART2DMA_Length;
		USART2DMA_Length = DMA_GetCurrDataCounter(DMA1_Stream5);//获取剩余长度
		USART2DMA_Length = 256-USART2DMA_Length; 

		if((USART2DMA_Length != USART2_templenth)&&(USART2_templenth==0))	//接收数据开始
		{
			USART2_Start = 0x55;
			USART2_ScanDelay = 0x05;
			USART2_ScanDelay_Flag = 0;
		}
	}
	
}
