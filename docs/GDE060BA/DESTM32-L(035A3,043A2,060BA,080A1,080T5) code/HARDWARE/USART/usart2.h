#ifndef __USART2_H
#define __USART2_H	

#include "sys.h" 



//控制字宏定义
#define WRITE_CMD				0x04

#define Data_Cmd	USART2_Buffer_Rx[12]
#define Data_D0		USART2_Buffer_Rx[13]
#define Data_D1		USART2_Buffer_Rx[14]
//通信帧中每一个数据项的数据长度宏定义
#define LEN_LENTH				0x04					//数据长度
#define ADRR_LENTH			0x07					//地址长度
#define CMD_LENTH				0x01					//控制字长度
#define ID_LENTH				0X02					//数据标识长度

#define DATA_START				LEN_LENTH+ADRR_LENTH+CMD_LENTH+ID_LENTH+1				//通信帧中数据的起始字节
#define PICTURE_LENTH		128						//数据标识长度		按照64*64点阵，800*600最多能显示108字节，汉字编码字节数为108*2

//#define ESL8

#ifdef ESL8
	#define picture_len_max	512
	#define picture_item_max	6140
#else
	#define picture_len_max	256
	#define picture_item_max	12280
#endif

extern unsigned char USART2_Buffer_Rx[256];
extern unsigned char USART2DMA_Length;
extern unsigned char USART2_templenth,USART2_Start;
extern unsigned char USART2_ScanDelay,USART2_ScanDelay_Flag;
extern unsigned long Item_Font;
extern unsigned char pic_rx_flag;			//显示内容接收进行中的标志
extern unsigned char pic_rx_item;			//显示内容接收进行中的第几条数据
extern unsigned char font_erase_flag;
extern unsigned char disp_update_flag;

void MyLib_UsartConfig(USART_TypeDef* USARTx,uint32_t BaudRate,uint16_t WordLength,uint16_t StopBits,uint16_t Parity);
void MyLib_UsartDMARx(USART_TypeDef* USARTx,DMA_Stream_TypeDef *DMA_Streamx,u32 Channel,u32 Memory0BaseAddr,u16 BufferSize,u32 DMA_Mode);
void MyLib_UsartDMATx(USART_TypeDef* USARTx,DMA_Stream_TypeDef *DMA_Streamx,u32 Channel,u32 Memory0BaseAddr,u16 BufferSize,u32 DMA_Mode);
void MYDMA_Enable(DMA_Stream_TypeDef *DMA_Streamx,u16 BufferSize);
void MYUSART_Init(void);
void USART2_Oper(void);
unsigned int CHAR_INT(unsigned char * pointchange);

#endif
