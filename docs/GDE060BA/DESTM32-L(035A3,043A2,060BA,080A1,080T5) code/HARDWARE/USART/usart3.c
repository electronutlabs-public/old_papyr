#include "usart3.h"
#include "usart2.h"
#include "MyLib_w32x16.h"
#include "MyLib_w32x16aux.h"
#include "mathchange.h"


unsigned char USART3_Buffer_Rx[256];
unsigned char USART3_Tx[20];
unsigned char USART3DMA_Length=0;
unsigned char USART3_templenth,USART3_Start;
unsigned char USART3_ScanDelay,USART3_ScanDelay_Flag;


unsigned char USART1_Buffer_Rx[256];
unsigned char USART1_Tx[20];
unsigned char USART1DMA_Length=0;
unsigned char USART1_templenth,USART1_Start;
unsigned char USART1_ScanDelay,USART1_ScanDelay_Flag;



unsigned char UART4_Buffer_Rx[256];
unsigned char UART4_Tx[20];
unsigned char UART4DMA_Length=0;
unsigned char UART4_templenth,UART4_Start;
unsigned char UART4_ScanDelay,UART4_ScanDelay_Flag;


/****************************USART通信处理*******************************/
/*************************************************************************
数据格式：

0xFE LEN0...LEN3 Adrr1.....Adrr6 Cmd Data1.....Datan Check 0x16

0xFE	起始符

LEN		数据长度4个字节 (包括Adrr  Cmd  Data)

Adrr	7字节地址

Cmd		控制命令

Data	数据域

Check 校验和

0x16	结束符

**************************************************************************/

/*********************************************************************************************************
** Function name:           USART3_Oper
**
** Descriptions:            串口2通信处理
**
** input parameters:        
** output parameters:       
**                          
** Returned value:          

*********************************************************************************************************/

void USART3_Oper(void)
{
	unsigned char kk_test[6],i;
	
	if(USART3_Start == 0x55)
	{
		if(USART3_ScanDelay_Flag == 0x55)
		{
			USART3_templenth =  USART3DMA_Length;
			USART3DMA_Length = DMA_GetCurrDataCounter(DMA1_Stream1);//获取剩余长度
			USART3DMA_Length = 256-USART3DMA_Length; 

			if(USART3DMA_Length != USART3_templenth) 			//延时5ms，检测是否有新的数据，如果有，重新开始延时
			{
				USART3_ScanDelay = 0x05;
				USART3_ScanDelay_Flag = 0;
			}
			else										//延时后如果没有新数据，初始化DMA，清延时计数器，清延时时间到的flag，清接收开始flag
			{	
				DMA_Cmd(DMA1_Stream1, DISABLE);
				
				USART3_ScanDelay = 0x00;
				USART3_ScanDelay_Flag = 0;
				USART3_Start = 0;
				

//				
//				DMA_Cmd(DMA1_Stream1, DISABLE);
				
				kk_test[0] = 0X01;		//Adress
				kk_test[1] = 0X01;		//cmd
				kk_test[2] = USART3_Buffer_Rx[2];		//帧系序列   需跟收到的数据一致
				kk_test[3] = 0X00;		//数据长度0
				kk_test[4] = 0X00;		//数据域
				kk_test[5] = 0X00;
				
				for(i=0;i<5;i++)
					kk_test[5] += kk_test[i];		
				kk_test[5] = 0xff-kk_test[5]+1;
					
//				MyLib_W32X_Flash_Byte_Write(W32X16,Font_Adrr+Item_Font*128,&USART3_Buffer_Rx[4],USART3_Buffer_Rx[3]);

			
				MyLib_UsartDMARx(USART3,DMA1_Stream1,DMA_Channel_4,(u32)USART3_Buffer_Rx,256,DMA_Mode_Normal);
				
				MyLib_UsartDMATx(USART3,DMA1_Stream3,DMA_Channel_4,(u32)kk_test,6,DMA_Mode_Normal);	
				
//				USART3_ScanDelay = 0x00;
//				USART3_ScanDelay_Flag = 0;
//				USART3_Start = 0;
			}
		}
	}
	else															//如果接收没有开始 
	{
		USART3_templenth =  USART3DMA_Length;
		USART3DMA_Length = DMA_GetCurrDataCounter(DMA1_Stream1);//获取剩余长度
		USART3DMA_Length = 256-USART3DMA_Length; 

		if((USART3DMA_Length != USART3_templenth)&&(USART3_templenth==0))	//接收数据开始
		{
			USART3_Start = 0x55;
			USART3_ScanDelay = 0x05;
			USART3_ScanDelay_Flag = 0;
		}
	}
	
}

void USART1_Oper(void)
{
	unsigned char kk_test[6],i;
	
	if(USART1_Start == 0x55)
	{
		if(USART1_ScanDelay_Flag == 0x55)
		{
			USART1_templenth =  USART1DMA_Length;
			USART1DMA_Length = DMA_GetCurrDataCounter(DMA2_Stream5);//获取剩余长度
			USART1DMA_Length = 256-USART1DMA_Length; 

			if(USART1DMA_Length != USART1_templenth) 			//延时5ms，检测是否有新的数据，如果有，重新开始延时
			{
				USART1_ScanDelay = 0x05;
				USART1_ScanDelay_Flag = 0;
			}
			else										//延时后如果没有新数据，初始化DMA，清延时计数器，清延时时间到的flag，清接收开始flag
			{	
				DMA_Cmd(DMA2_Stream5, DISABLE);
				
				USART1_ScanDelay = 0x00;
				USART1_ScanDelay_Flag = 0;
				USART1_Start = 0;
				

//				
//				DMA_Cmd(DMA1_Stream1, DISABLE);
				
				kk_test[0] = 0X01;		//Adress
				kk_test[1] = 0X01;		//cmd
				kk_test[2] = USART1_Buffer_Rx[2];		//帧系序列   需跟收到的数据一致
				kk_test[3] = 0X00;		//数据长度0
				kk_test[4] = 0X00;		//数据域
				kk_test[5] = 0X00;
				
				for(i=0;i<5;i++)
					kk_test[5] += kk_test[i];		
				kk_test[5] = 0xff-kk_test[5]+1;
					
//				MyLib_W32X_Flash_Byte_Write(W32X16,Font_Adrr+Item_Font*128,&USART3_Buffer_Rx[4],USART3_Buffer_Rx[3]);

			
				MyLib_UsartDMARx(USART1,DMA2_Stream5,DMA_Channel_4,(u32)USART1_Buffer_Rx,256,DMA_Mode_Normal);
				
				MyLib_UsartDMATx(USART1,DMA2_Stream7,DMA_Channel_4,(u32)kk_test,6,DMA_Mode_Normal);	
				
//				USART3_ScanDelay = 0x00;
//				USART3_ScanDelay_Flag = 0;
//				USART3_Start = 0;
			}
		}
	}
	else															//如果接收没有开始 
	{
		USART1_templenth =  USART1DMA_Length;
		USART1DMA_Length = DMA_GetCurrDataCounter(DMA2_Stream5);//获取剩余长度
		USART1DMA_Length = 256-USART1DMA_Length; 

		if((USART1DMA_Length != USART1_templenth)&&(USART1_templenth==0))	//接收数据开始
		{
			USART1_Start = 0x55;
			USART1_ScanDelay = 0x05;
			USART1_ScanDelay_Flag = 0;
		}
	}
	
}


void UART4_Oper(void)
{
	unsigned char kk_test[6],i;
	
	if(UART4_Start == 0x55)
	{
		if(UART4_ScanDelay_Flag == 0x55)
		{
			UART4_templenth =  UART4DMA_Length;
			UART4DMA_Length = DMA_GetCurrDataCounter(DMA1_Stream2);//获取剩余长度
			UART4DMA_Length = 256-UART4DMA_Length; 

			if(UART4DMA_Length != UART4_templenth) 			//延时5ms，检测是否有新的数据，如果有，重新开始延时
			{
				UART4_ScanDelay = 0x05;
				UART4_ScanDelay_Flag = 0;
			}
			else										//延时后如果没有新数据，初始化DMA，清延时计数器，清延时时间到的flag，清接收开始flag
			{	
				DMA_Cmd(DMA1_Stream2, DISABLE);
				
				UART4_ScanDelay = 0x00;
				UART4_ScanDelay_Flag = 0;
				UART4_Start = 0;
				

//				
//				DMA_Cmd(DMA1_Stream1, DISABLE);
				
				kk_test[0] = 0X01;		//Adress
				kk_test[1] = 0X01;		//cmd
				kk_test[2] = UART4_Buffer_Rx[2];		//帧系序列   需跟收到的数据一致
				kk_test[3] = 0X00;		//数据长度0
				kk_test[4] = 0X00;		//数据域
				kk_test[5] = 0X00;
				
				for(i=0;i<5;i++)
					kk_test[5] += kk_test[i];		
				kk_test[5] = 0xff-kk_test[5]+1;
					
//				MyLib_W32X_Flash_Byte_Write(W32X16,Font_Adrr+Item_Font*128,&USART3_Buffer_Rx[4],USART3_Buffer_Rx[3]);

			
				MyLib_UsartDMARx(UART4,DMA1_Stream2,DMA_Channel_4,(u32)UART4_Buffer_Rx,256,DMA_Mode_Normal);
				
				MyLib_UsartDMATx(UART4,DMA1_Stream4,DMA_Channel_4,(u32)kk_test,6,DMA_Mode_Normal);	
				
//				USART3_ScanDelay = 0x00;
//				USART3_ScanDelay_Flag = 0;
//				USART3_Start = 0;
			}
		}
	}
	else															//如果接收没有开始 
	{
		UART4_templenth =  UART4DMA_Length;
		UART4DMA_Length = DMA_GetCurrDataCounter(DMA1_Stream2);//获取剩余长度
		UART4DMA_Length = 256-UART4DMA_Length; 

		if((UART4DMA_Length != UART4_templenth)&&(UART4_templenth==0))	//接收数据开始
		{
			UART4_Start = 0x55;
			UART4_ScanDelay = 0x05;
			UART4_ScanDelay_Flag = 0;
		}
	}
	
}


