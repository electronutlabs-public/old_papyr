#include "key.h"
#include "delay.h" 
#include "epaper.h"	
#include "array43.h"	
#include "array35.h"	
#include "array60.h"	
#include "array80.h"	

//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//按键输入驱动代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2014/5/3
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved									  
////////////////////////////////////////////////////////////////////////////////// 	 
unsigned char key1_delay,key2_delay,key3_delay,key4_delay;
unsigned char bat_check_delay=0;
unsigned char bat_check_flag=0;
unsigned char keyinput_flag=0;
unsigned char key_disp_update=0;
unsigned char start_flag=0x55;
unsigned char epaper_flag=0;				//电子纸尺寸标致
unsigned char epaper_flag_temp=0;				//
unsigned char update_flag=0;				//显示更新标标志
//按键初始化函数
void KEY_Init(void)
{
	
	GPIO_InitTypeDef  GPIO_InitStructure;
  
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);//使能GPIOF时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);//使能GPIOF时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);//使能GPIOF时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);//使能GPIOF时钟
	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7; //BAT_CTL PG7
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//浮空输入
  GPIO_Init(GPIOG, &GPIO_InitStructure);//
	GPIO_SetBits(GPIOG,GPIO_Pin_7);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6; //BAT_LED PG6
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//浮空输入
  GPIO_Init(GPIOG, &GPIO_InitStructure);//
	GPIO_ResetBits(GPIOG,GPIO_Pin_6);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8; //BAT_KEY_CHECK  PG8
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;//浮空输入
  GPIO_Init(GPIOG, &GPIO_InitStructure);//
	

	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7; //KEY1  PD7
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOD, &GPIO_InitStructure);//
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2; //KEY2  PE2
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOE, &GPIO_InitStructure);//
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8; //KEY3  PA8
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOA, &GPIO_InitStructure);//
	
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11; //KEY4  PA11
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOA, &GPIO_InitStructure);//
	
	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6; //KEY_LED1  PD6
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输入模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//上拉
  GPIO_Init(GPIOD, &GPIO_InitStructure);//
	GPIO_SetBits(GPIOD,GPIO_Pin_6);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15; //KEY_LED2  PG15
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输入模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//上拉
  GPIO_Init(GPIOG, &GPIO_InitStructure);//
	GPIO_SetBits(GPIOG,GPIO_Pin_15);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //KEY_LED3  PG9
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输入模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//上拉
  GPIO_Init(GPIOG, &GPIO_InitStructure);//
	GPIO_SetBits(GPIOG,GPIO_Pin_9);
	
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12; //KEY_LED4  PA12
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输入模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//上拉
  GPIO_Init(GPIOA, &GPIO_InitStructure);//
	GPIO_SetBits(GPIOA,GPIO_Pin_12);
		 
} 

void LED_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);//使能GPIOF时钟
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12; //LED1 PB12
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//
  GPIO_Init(GPIOB, &GPIO_InitStructure);//
	GPIO_SetBits(GPIOB,GPIO_Pin_12);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13; //LED1 PB13
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//
  GPIO_Init(GPIOB, &GPIO_InitStructure);//
	GPIO_SetBits(GPIOB,GPIO_Pin_13);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14; //LED1 PB14
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//
  GPIO_Init(GPIOB, &GPIO_InitStructure);//
	GPIO_SetBits(GPIOB,GPIO_Pin_14);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15; //LED1 PB15
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//
  GPIO_Init(GPIOB, &GPIO_InitStructure);//
	GPIO_SetBits(GPIOB,GPIO_Pin_15);
}

void DIP_Switch_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; //PB0
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOB, &GPIO_InitStructure);//
	
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4; //PB4
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
//  GPIO_Init(GPIOB, &GPIO_InitStructure);//
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5; //PB5
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOB, &GPIO_InitStructure);//
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6; //PB6
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOB, &GPIO_InitStructure);//

}


void deal_key1(void)							//flash片擦写
{
//	MyLib_W32X_ChipErase(W32X16);	

//		if((GPIO_ReadOutputDataBit(GPIOB,GPIO_Pin_12)) == (uint8_t)Bit_RESET)
//			GPIO_SetBits(GPIOB,GPIO_Pin_12);
//		else
//		GPIO_ResetBits(GPIOB,GPIO_Pin_12);		
	if((GPIOB->ODR & GPIO_Pin_12) == (uint32_t)Bit_RESET)
		GPIO_SetBits(GPIOB,GPIO_Pin_12);
	else
		GPIO_ResetBits(GPIOB,GPIO_Pin_12);
	
	if((GPIOD->ODR & GPIO_Pin_6) == (uint32_t)Bit_RESET)
		GPIO_SetBits(GPIOD,GPIO_Pin_6);
	else
		GPIO_ResetBits(GPIOD,GPIO_Pin_6);
	
}

void deal_key2(void)
{
	if((GPIOB->ODR & GPIO_Pin_13) == (uint32_t)Bit_RESET)
		GPIO_SetBits(GPIOB,GPIO_Pin_13);
	else
		GPIO_ResetBits(GPIOB,GPIO_Pin_13);
	
	if((GPIOG->ODR & GPIO_Pin_15) == (uint32_t)Bit_RESET)
		GPIO_SetBits(GPIOG,GPIO_Pin_15);
	else
		GPIO_ResetBits(GPIOG,GPIO_Pin_15);


}

void deal_key3(void)
{
	if((GPIOB->ODR & GPIO_Pin_14) == (uint32_t)Bit_RESET)
		GPIO_SetBits(GPIOB,GPIO_Pin_14);
	else
		GPIO_ResetBits(GPIOB,GPIO_Pin_14);
	
	if((GPIOG->ODR & GPIO_Pin_9) == (uint32_t)Bit_RESET)
		GPIO_SetBits(GPIOG,GPIO_Pin_9);
	else
		GPIO_ResetBits(GPIOG,GPIO_Pin_9);


}

void deal_key4(void)							//切换图片
{
//	item_dis++;
//	if(item_dis>=picture_item_max)
//		item_dis = 0;
//	disp_update_flag = 0x55;
//	key_disp_update = 0x55;
	
	if((GPIOB->ODR & GPIO_Pin_15) == (uint32_t)Bit_RESET)
		GPIO_SetBits(GPIOB,GPIO_Pin_15);
	else
		GPIO_ResetBits(GPIOB,GPIO_Pin_15);
	
	if((GPIOA->ODR & GPIO_Pin_12) == (uint32_t)Bit_RESET)
		GPIO_SetBits(GPIOA,GPIO_Pin_12);
	else
		GPIO_ResetBits(GPIOA,GPIO_Pin_12);
}

//按键处理函数
void deal_key(void)
{	 
	if(keyinput_flag != 0)
	{
		if(keyinput_flag&k1_flag)
			deal_key1();	
		if(keyinput_flag&k2_flag)
			deal_key2();
		if(keyinput_flag&k3_flag)
			deal_key3();		
		if(keyinput_flag&k4_flag)
			deal_key4();
		
		keyinput_flag = 0;
	
	}
	if(bat_check_flag == 0x55)
	{
		//		GPIOG->BSRRL = GPIO_Pin_8;
		GPIO_ResetBits(GPIOG,GPIO_Pin_7);
		bat_check_flag = 0;	
	}
}


void Judge_Display(void)
{
	unsigned char display_flag = 0;
	unsigned int i,j;
	unsigned char (*pr_begin_34)[18];
	unsigned char (*pr_end_34)[26];
	unsigned long *pr_init_34;
	unsigned char (*pr_begin_68)[8];
	unsigned char (*pr_end_68)[18];
	unsigned char *pr_init_68;

	
	if((GPIOB->IDR & GPIO_Pin_0) == (uint32_t)Bit_RESET)
		display_flag |= 0x01;
	else
		display_flag &= ~0x01;
	
	if((GPIOB->IDR & GPIO_Pin_5) == (uint32_t)Bit_RESET)
		display_flag |= 0x02;
	else
		display_flag &= ~0x02;

	if((GPIOB->IDR & GPIO_Pin_6) == (uint32_t)Bit_RESET)
		display_flag |= 0x04;
	else
		display_flag &= ~0x04;

	if((GPIOB->IDR & GPIO_Pin_4) == (uint32_t)Bit_RESET)
		display_flag |= 0x08;
	else
		display_flag &= ~0x08;	
	
//	display_flag = 0x01;
	
	
	
	switch(display_flag)
	{
		case 0x01:
				epaper_flag = ESL43;
				FRAME_BEGIN_LEN = FRAME_BEGIN_LEN_43;
				FRAME_END_LEN = FRAME_END_LEN_43;
				FRAME_INIT_LEN = FRAME_INIT_LEN_43;
		
				pr_begin_34 = wave_begin_43;
				pr_end_34 = wave_end_43;		
				pr_init_34 = wave_init_43;
				
				epaper_line = 800;
				epaper_row = 600;
				GPIOB->ODR = ((GPIOB->ODR & 0xefff) | 0xe000);
				break;

		case 0x02:
				epaper_flag = ESL35;
				FRAME_BEGIN_LEN = FRAME_BEGIN_LEN_35;
				FRAME_END_LEN = FRAME_END_LEN_35;
				FRAME_INIT_LEN = FRAME_INIT_LEN_35;
		
				pr_begin_34 = wave_begin_35;
				pr_end_34 = wave_end_35;		
				pr_init_34 = wave_init_35;

				epaper_line = 800;
				epaper_row = 480;
				GPIOB->ODR = ((GPIOB->ODR & 0xdfff) | 0xd000);
				break;

		case 0x04:
				epaper_flag = ESL60;		
		
				FRAME_BEGIN_LEN = FRAME_BEGIN_LEN_60;
				FRAME_END_LEN = FRAME_END_LEN_60;
				FRAME_INIT_LEN = FRAME_INIT_LEN_60;
	
				pr_begin_68 = wave_begin_60;
				pr_end_68 = wave_end_60;		
				pr_init_68 = wave_init_60;

				epaper_line = 800;
				epaper_row = 600;
	
				GPIOB->ODR = ((GPIOB->ODR & 0xbfff) | 0xb000);
				break;

		case 0x08:
				epaper_flag = ESL80;
				FRAME_BEGIN_LEN = FRAME_BEGIN_LEN_80;
				FRAME_END_LEN = FRAME_END_LEN_80;
				FRAME_INIT_LEN = FRAME_INIT_LEN_80;
		
				pr_begin_68 = wave_begin_80;
				pr_end_68 = wave_end_80;		
				pr_init_68 = wave_init_80;
		
				epaper_line = 1024;
				epaper_row = 768;
				GPIOB->ODR = ((GPIOB->ODR & 0x7fff) | 0x7000);
				break;

		default:
				if(epaper_flag == 0)
				{
					epaper_flag = ESL43;
					FRAME_BEGIN_LEN = FRAME_BEGIN_LEN_43;
					FRAME_END_LEN = FRAME_END_LEN_43;
					FRAME_INIT_LEN = FRAME_INIT_LEN_43;
					
					pr_begin_34 = wave_begin_43;
					pr_end_34 = wave_end_43;		
					pr_init_34 = wave_init_43;
					
					epaper_line = 800;
					epaper_row = 600;
					GPIOB->ODR = ((GPIOB->ODR & 0xe000) | 0xe000);
				}
				break;				
	}

	
	if(epaper_flag != epaper_flag_temp)
		update_flag = 0x55;
	else
		update_flag = 0;
	
	epaper_flag_temp = epaper_flag;
	
	if((epaper_flag == ESL43) ||(epaper_flag == ESL35))
	{
		for(i=0;i<4;i++)
		{
			for(j=0;j<FRAME_BEGIN_LEN;j++)
			{
				wave_begin[i][j] = *(pr_begin_34[i]+j);					
			}
		}
		for(i=0;i<4;i++)
		{
			for(j=0;j<FRAME_END_LEN;j++)
			{
				wave_end[i][j] = *(pr_end_34[i]+j);					
			}
		}
		for(i=0;i<FRAME_INIT_LEN;i++)
			wave_init[i] = *(pr_init_34+i);		
	}	
	else if((epaper_flag == ESL60) ||(epaper_flag == ESL80))
	{
		for(i=0;i<4;i++)
		{
			for(j=0;j<FRAME_BEGIN_LEN;j++)
			{
				wave_begin[i][j] = *(pr_begin_68[i]+j);					
			}
		}
		for(i=0;i<4;i++)
		{
			for(j=0;j<FRAME_END_LEN;j++)
			{
				wave_end[i][j] = *(pr_end_68[i]+j);					
			}
		}
		for(i=0;i<FRAME_INIT_LEN;i++)
			wave_init[i] = *(pr_init_68+i);		
	}

}



