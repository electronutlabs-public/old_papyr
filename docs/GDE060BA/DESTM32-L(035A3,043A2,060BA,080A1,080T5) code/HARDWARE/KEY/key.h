#ifndef __KEY_H
#define __KEY_H	 
#include "sys.h" 
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//按键输入驱动代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2014/5/3
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved									  
////////////////////////////////////////////////////////////////////////////////// 	 



#define k1_flag		0x01
#define k2_flag		0x02
#define k3_flag		0x04
#define k4_flag		0x08
#define k5_flag		0x10
#define k6_flag		0x20
#define k7_flag		0x40
#define k8_flag		0x80

#define ESL43		0x01
#define ESL35		0x02
#define ESL60		0x04
#define ESL80		0x08





extern unsigned char key1_delay,key2_delay,key3_delay,key4_delay;
extern unsigned char bat_check_delay;
extern unsigned char bat_check_flag;
extern unsigned char keyinput_flag;
extern unsigned char key_disp_update;
extern unsigned char start_flag;
extern unsigned char epaper_flag;				//电子纸尺寸标致
extern unsigned char epaper_flag_temp;				//电子纸尺寸标致
extern unsigned char update_flag;				//显示更新标标志

void KEY_Init(void);	//IO初始化
u8 KEY_Scan(u8);  		//按键扫描函数	
void deal_key(void);
void LED_Init(void);
void DIP_Switch_Init(void);
void Judge_Display(void);

#endif
